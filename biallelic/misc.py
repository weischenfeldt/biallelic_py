import os
import sys
import imp
import gzip
import argparse
import datetime
import secrets
import string
import types
import importlib.machinery
from biallelic.bgzf import BgzfWriter, BgzfReader


def make_ucsc_format(chrom: str, start: int, end: int):
    if chrom.startswith("chr"):
        ucsc_format_str = "%s:%i-%i"
    else:
        ucsc_format_str = "chr%s:%i-%i"
    return ucsc_format_str % (chrom, start, end)


def camel_case_split(str):
    words = [[str[0]]]
    for c in str[1:]:
        if words[-1][-1].islower() and c.isupper():
            words.append(list(c))
        else:
            words[-1].append(c)
    return ["".join(word) for word in words]


def import_module(module_name, module_path):
    loader = importlib.machinery.SourceFileLoader(module_name, module_path)
    mod = types.ModuleType(loader.name)
    loader.exec_module(mod)
    return mod


def generate_uid(n=4):
    alphabet = string.ascii_uppercase + string.digits
    random_str = "".join([secrets.choice(alphabet) for _ in range(n)])
    random_str = "%s_%s" % (
        datetime.datetime.now().strftime("%y%m%d%H%M%S.%f"),
        random_str,
    )
    return random_str

def color_palettes(x="default"):
    
    pal ={'germ_snp/som_loss': '#768b02',
        'som_cn_loh/som_snv': '#8b6a54',
        'som_gain_loh/som_snv': '#aa96b1',
        'som_loss/methyl': '#d5b28a',
        'som_loss/som_indel': '#80997f',
        'som_loss/som_loss': '#d56e67',
        'som_loss/som_snv': '#c6c0ac',
        'som_loss/som_sv': '#5a4b67',
        'som_snv/som_snv': '#2c4c68',
        'som_loss/subclonal_snv': '#a05a6e'}
    return pal


def package_modules(package):
    pathname = package.__path__[0]
    return {
        ".".join([package.__name__, os.path.splitext(module)[0]])
        for module in os.listdir(pathname)
        if module.endswith(".py") and not module.startswith("__init__")
    }


def try_import(path, module_name):
    if not os.path.isdir(os.path.join(path, module_name)):
        os.makedirs(os.path.join(path, module_name))
    try:
        mod = imp.load_module(
            module_name, None, os.path.join(path, module_name), ("", "", 5)
        )
    except ValueError:
        init_path = os.path.join(path, module_name, "__init__.py")
        try:
            with open(init_path, "a"):
                os.utime(init_path, None)
            mod = imp.load_module(
                module_name, None, os.path.join(path, module_name), ("", "", 5)
            )
        except IOError:
            raise Exception(
                (
                    "There is no directory in the path %s. "
                    "Check your configuration or create "
                    "the directory in the desired path."
                )
                % os.path.join(path, module_name)
            )
    return mod


def get_modules_names(parent):
    mods = package_modules(parent)
    modules = []
    for mod in mods:
        try:
            __import__(mod)
            mod_name = mod.split(".")[-1]
            modules.append(mod_name)
        except AttributeError:
            pass
    return modules


def get_module_method(parent, module, method):
    mods = package_modules(parent)
    result = None
    for mod in mods:
        try:
            __import__(mod)
            mod_name = mod.split(".")[-1]
            if mod_name == module:
                m = getattr(parent, mod_name)
                result = getattr(m, method)
                break
        except AttributeError:
            pass
    return result


def xopen(filename, mode="r", bgzip=False):
    """
    Replacement for the "open" function that can also open
    files that have been compressed with gzip. If the filename ends with .gz,
    the file is opened with gzip.open(). If it doesn't, the regular open()
    is used. If the filename is '-', standard output (mode 'w') or input
    (mode 'r') is returned.
    """
    assert isinstance(filename, str)
    if filename == "-":
        return sys.stdin if "r" in mode else sys.stdout
    if bgzip:
        if mode.startswith("w"):
            return BgzfWriter(filename, mode)
        elif mode.startswith("r"):
            return BgzfReader(filename, mode)
    if filename.endswith(".gz"):
        return gzip.open(filename, mode)
    else:
        return open(filename, mode)


# pp = xopen("/home/projects/cu_10027/data/projects/ppcg/data/data_processed/ppcg_data_release/Germline_variants_2/ppcg.snv_indel.germline.v1.bcf", "r", bgzip=True)
# print(next(pp).decode("utf-8"))


class SubcommandHelpFormatter(argparse.RawDescriptionHelpFormatter):
    def _format_action(self, action):
        parts = super()._format_action(action)
        if action.nargs == argparse.PARSER:
            parts = "\n".join(parts.split("\n")[1:])
        return parts


class DefaultHelpParser(argparse.ArgumentParser):
    def error(self, message):
        sys.stderr.write("error: %s\n" % message)
        self.print_help()
        sys.exit(2)
