from biallelic.misc import xopen
from biallelic.models import Aberration, AberrationType
import pandas as pd

aberration_headers = [
    "chrom",
    "start",
    "end",
    "type",
    "subtype",
    "sample_id",
    "vaf",
    "n_copy",
    "gene",
    "id",
]


def aberrations(file_name, logger, annotation):
    aberration_data = []

    logger.info("Read file %s as Aberrations format" % file_name)
    with xopen(file_name, "rt") as input_ab:
        header = next(input_ab).strip().split("\t")
        chromosome_idx = header.index("chrom")
        start_idx = header.index("start")
        end_idx = header.index("end")
        var_subtype_idx = header.index("subtype")
        var_type_idx = header.index("type")
        sample_idx = header.index("sample_id")
        n_copy_idx = header.index("n_copy")
        vaf_idx = header.index("vaf")
        tot_n = 0
        selected_n = 0
        for line in input_ab:
            tot_n += 1
            line = line.strip().split("\t")
            try:
                vaf = float(line[vaf_idx])
            except ValueError:
                vaf = None
            try:
                n_copy = int(line[n_copy_idx])
            except ValueError:
                n_copy = None
            selected_n += 1
            aberration_data.append(
                Aberration(
                    **{
                        "chrom": str(line[chromosome_idx]),
                        "start": int(line[start_idx]),
                        "end": int(line[end_idx]),
                        "aberration_type": str_to_type(line[var_type_idx]),
                        "aberration_subtype": line[var_subtype_idx],
                        "sample_id": line[sample_idx],
                        "vaf": vaf,
                        "n_copy": n_copy,
                        "gene": ".",
                        "id": ".",
                    }
                )
            )
        logger.info(
            "Selected %i aberrations over %i variants" % (selected_n, tot_n)
        )
        return pd.DataFrame(vars(a) for a in aberration_data)


def str_to_type(x: str):
    for i in range(1, len(AberrationType) + 1):
        if AberrationType(i).name == x:
            return AberrationType(i)
