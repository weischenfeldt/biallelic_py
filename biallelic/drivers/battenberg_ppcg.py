from biallelic.misc import xopen
from biallelic.models import Aberration, AberrationType
import pandas as pd
import os


BATTENBERG_HEADERS = [
    "chr",
    "startpos",
    "endpos",
    "BAF",
    "pval",
    "LogR",
    "ntot",
    "nMaj1_A",
    "nMin1_A",
    "frac1_A",
    "nMaj2_A",
    "nMin2_A",
    "frac2_A",
    "SDfrac_A",
    "SDfrac_A_BS",
    "frac1_A_0.025",
    "frac1_A_0.975",
    "nMaj1_B",
    "nMin1_B",
    "frac1_B",
    "nMaj2_B",
    "nMin2_B",
    "frac2_B",
    "SDfrac_B",
    "SDfrac_B_BS",
    "frac1_B_0.025",
    "frac1_B_0.975",
    "nMaj1_C",
    "nMin1_C",
    "frac1_C",
    "nMaj2_C",
    "nMin2_C",
    "frac2_C",
    "SDfrac_C",
    "SDfrac_C_BS",
    "frac1_C_0.025",
    "frac1_C_0.975",
    "nMaj1_D",
    "nMin1_D",
    "frac1_D",
    "nMaj2_D",
    "nMin2_D",
    "frac2_D",
    "SDfrac_D",
    "SDfrac_D_BS",
    "frac1_D_0.025",
    "frac1_D_0.975",
    "nMaj1_E",
    "nMin1_E",
    "frac1_E",
    "nMaj2_E",
    "nMin2_E",
    "frac2_E",
    "SDfrac_E",
    "SDfrac_E_BS",
    "frac1_E_0.025",
    "frac1_E_0.975",
    "nMaj1_F",
    "nMin1_F",
    "frac1_F",
    "nMaj2_F",
    "nMin2_F",
    "frac2_F",
    "SDfrac_F",
    "SDfrac_F_BS",
    "frac1_F_0.025",
    "frac1_F_0.975",
]


AMP_DIFF_LEVEL=4

def scna(file_name, logger, annotation, ploidy_file, min_ccf=0.05):
    ploidy_dict = {}
    aberration_data = []
    logger.info(
        "Collect ploidy and cellularity information from %s" % ploidy_file
    )
    with xopen(ploidy_file, "rt") as ploidy_info:
        header = next(ploidy_info).strip().split(",")
        sample_idx = header.index("PPCG_Sample_ID")
        cellularity_idx = header.index("Cellularity")
        ploidy_idx = header.index("ploidy")
        for sample_line in ploidy_info:
            sample_line = sample_line.strip().split(",")
            ploidy_dict[sample_line[sample_idx]] = {
                "ploidy": float(sample_line[ploidy_idx]),
                "cellularity": float(sample_line[cellularity_idx]),
            }
    if os.path.isdir(file_name):
        scna_files = os.listdir(file_name)
        logger.info("Load copy number data for %i samples" % len(scna_files))
        for scna_file in scna_files:
            scna_file_path = os.path.join(file_name, scna_file)
            sample_id = scna_file.split("_vs_")[0]
            try:
                ploidy = ploidy_dict[sample_id]["ploidy"]
                aberration_data += parse_battenberg_calls(
                    scna_file_path,
                    logger,
                    min_ccf,
                    ploidy,
                    sample_id,
                )
            except KeyError:
                logger.error(
                    "Could not find ploidy information for sample %s"
                    % sample_id
                )
        logger.info("Found %i aberrant segments" % len(aberration_data))
        return pd.DataFrame(vars(a) for a in aberration_data)


def parse_battenberg_calls(file_name, logger, min_ccf, ploidy, sample_id):
    aberration_data = []
    with xopen(file_name, "rt") as battenberg_calls:
        header = next(battenberg_calls).strip().split("\t")
        chrom_idx = header.index("chr")
        start_idx = header.index("startpos")
        end_idx = header.index("endpos")
        logR_idx = header.index("LogR")
        ccf_idx = header.index("frac1_A")
        major_CN_idx = header.index("nMaj1_A")
        minor_CN_idx = header.index("nMin1_A")
        for segment in battenberg_calls:
            aberration_subtype = "DEL"
            segment = segment.strip().split("\t")
            try:
                ccf = float(segment[ccf_idx])
            except ValueError:
                logger.error(
                    "Error formatting CCF value, skipping segments %s"
                    % "\t".join(segment)
                )
                continue
            try:
                logR = float(segment[logR_idx])
            except ValueError:
                logger.error(
                    "Error formatting LogR value, skipping segments %s"
                    % "\t".join(segment)
                )
                continue
            if ccf > min_ccf:
                try:
                    major_CN = float(segment[major_CN_idx])
                    minor_CN = float(segment[minor_CN_idx])
                except ValueError:
                    # logger.error(
                    #    "Error retrivinf copy number values, skipping segments %s"
                    #    % "\t".join(segment)
                    # )
                    continue
                aberration_type = None
                if segment[chrom_idx] != "X":
                    if minor_CN == 0:
                        if round(major_CN + minor_CN, 0) < round(ploidy, 0):
                            if major_CN == 0:
                                aberration_type = AberrationType.HOM_LOSS
                            else:
                                aberration_type = AberrationType.HET_LOSS
                        else:
                            if round(major_CN, 0) == round(ploidy, 0):
                                aberration_type = AberrationType.CNN_LOH
                            else:
                                aberration_type = AberrationType.GAIN_LOH
                    else:
                        if round(major_CN + minor_CN, 0) - round(ploidy, 0) > 0:
                            aberration_subtype = "GAIN"
                            if round(major_CN + minor_CN, 0) - round(ploidy, 0) > AMP_DIFF_LEVEL:
                                aberration_type = AberrationType.AMP
                            else:
                                aberration_type = AberrationType.GAIN
                else:
                    if round(major_CN, 0) < round(ploidy/2.0, 0):
                        if major_CN == 0:
                            aberration_type = AberrationType.HOM_LOSS
                        else:
                            aberration_type = AberrationType.HET_LOSS

                    elif round(major_CN, 0) - round(ploidy/2.0, 0) > 0:
                        aberration_subtype = "GAIN"
                        if round(major_CN, 0) - round(ploidy/2.0, 0) > AMP_DIFF_LEVEL/2.0:
                            aberration_type = AberrationType.AMP
                        else:
                            aberration_type = AberrationType.GAIN

                if aberration_type is not None:
                    aberration_data.append(
                        Aberration(
                            **{
                                "chrom": segment[chrom_idx],
                                "start": int(segment[start_idx]),
                                "end": int(segment[end_idx]),
                                "aberration_type": aberration_type,
                                "aberration_subtype": aberration_subtype,
                                "sample_id": sample_id,
                                "vaf": ccf,
                                "n_copy": major_CN,
                            }
                        )
                    )
    return aberration_data
