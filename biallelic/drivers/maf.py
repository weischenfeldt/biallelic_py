from biallelic.misc import xopen
from biallelic.models import (
    Aberration,
    AberrationType,
    OmicsType,
    SampleDonor,
    Gender,
)
import pandas as pd


MAF_HEADERS = [
    "Hugo_Symbol",
    "Entrez_Gene_Id",
    "Center",
    "NCBI_Build",
    "Chromosome",
    "Start_Position",
    "End_Position",
    "Strand",
    "Consequence",
    "Variant_Classification",
    "Variant_Type",
    "Reference_Allele",
    "Tumor_Seq_Allele1",
    "Tumor_Seq_Allele2",
    "dbSNP_RS",
    "dbSNP_Val_Status",
    "Tumor_Sample_Barcode",
    "Matched_Norm_Sample_Barcode",
    "Match_Norm_Seq_Allele1",
    "Match_Norm_Seq_Allele2",
    "Tumor_Validation_Allele1",
    "Tumor_Validation_Allele2",
    "Match_Norm_Validation_Allele1",
    "Match_Norm_Validation_Allele2",
    "Verification_Status",
    "Validation_Status",
    "Mutation_Status",
    "Sequencing_Phase",
    "Sequence_Source",
    "Validation_Method",
    "Score",
    "BAM_File",
    "Sequencer",
    "t_ref_count",
    "t_alt_count",
    "n_ref_count",
    "n_alt_count",
    "HGVSc",
    "HGVSp",
    "HGVSp_Short",
    "Transcript_ID",
    "RefSeq",
    "Protein_position",
    "Codons",
    "Hotspot",
    "NCALLERS",
    "ALLELE_NUM",
    "PICK",
    "UNIPARC",
    "n_depth",
    "Feature",
    "CONTEXT",
    "CLIN_SIG",
    "Gene",
    "HGNC_ID",
    "MERGESOURCE",
    "ExAC_AF_AMR",
    "t_depth",
    "DISTANCE",
    "SYMBOL_SOURCE",
    "Existing_variation",
    "SYMBOL",
    "ExAC_AF_SAS",
    "VARIANT_CLASS",
    "AA_MAF",
    "HIGH_INF_POS",
    "GENE_PHENO",
    "ExAC_AF_AFR",
    "ASN_MAF",
    "PHENO",
    "BIOTYPE",
    "AFR_MAF",
    "DOMAINS",
    "MOTIF_SCORE_CHANGE",
    "Amino_acids",
    "EA_MAF",
    "Allele",
    "cDNA_position",
    "ExAC_AF_NFE",
    "SIFT",
    "INTRON",
    "TREMBL",
    "AMR_MAF",
    "EAS_MAF",
    "CANONICAL",
    "DBVS",
    "all_effects",
    "ExAC_AF_EAS",
    "GMAF",
    "MOTIF_NAME",
    "TSL",
    "SOMATIC",
    "MOTIF_POS",
    "IMPACT",
    "CDS_position",
    "COSMIC",
    "SWISSPROT",
    "ExAC_AF_FIN",
    "EUR_MAF",
    "Feature_type",
    "HGVS_OFFSET",
    "PolyPhen",
    "FILTER",
    "ENSP",
    "ExAC_AF",
    "CENTERS",
    "CCDS",
    "EXON",
    "ExAC_AF_OTH",
    "SAS_MAF",
    "Exon_Number",
    "MINIMISED",
    "PUBMED",
]

VARIANT_CLASSIFICATION = [
    "Intron",
    "5'UTR",
    "3'UTR",
    "5'Flank",
    "3'Flank",
    "IGR",
    "Frame_Shift_Del",
    "Frame_Shift_Ins",
    "In_Frame_Del",
    "In_Frame_Ins",
    "Missense_Mutation",
    "Nonsense_Mutation",
    "Silent",
    "Splice_Site",
    "Translation_Start_Site",
    "Nonstop_Mutation",
    "RNA",
    "Targeted_Region",
]


def sample_donors(file_path, logger):
    donors_info = []
    samples_ids = []
    logger.info("Read Sample/Donor information from %s" % file_path)
    with xopen(file_path, "rt") as maf_info:
        header = next(maf_info).strip().split("\t")
        sample_id_idx = header.index("Tumor_Sample_Barcode")
        donor_id_idx = header.index("Matched_Norm_Sample_Barcode")
        for line in maf_info:
            line = line.strip().split("\t")
            if line[sample_id_idx] not in samples_ids:
                donors_info.append(
                    SampleDonor(
                        sample_id=line[sample_id_idx],
                        donor_id=line[donor_id_idx],
                        gender=Gender.Unknown,
                        omics=OmicsType.Genomics,
                    )
                )
                samples_ids.append(line[sample_id_idx])
    logger.info("Recorded %i Sample/Donor information" % len(donors_info))
    return pd.DataFrame(vars(d) for d in donors_info)


def snv(file_name, logger, annotation, min_scaled_vaf=0.05):
    aberration_data = []
    damaging_mutations = [
        "Frame_Shift_Del",
        "Frame_Shift_Ins",
        "In_Frame_Del",
        "In_Frame_Ins",
        "Missense_Mutation",
        "Nonsense_Mutation",
        "Splice_Site",
        "Translation_Start_Site",
        "Nonstop_Mutation",
    ]
    logger.info("Read file %s as MAF format" % file_name)
    sample_donor = annotation["sample_donors"].set_index("sample_id")
    with xopen(file_name, "rt") as input_snv:
        header = next(input_snv).strip().split("\t")
        chromosome_idx = header.index("Chromosome")
        start_idx = header.index("Start_Position")
        end_idx = header.index("End_Position")
        gene_idx = header.index("Hugo_Symbol")
        var_class_idx = header.index("Variant_Classification")
        var_type_idx = header.index("Variant_Type")
        t_ref_count_idx = header.index("t_ref_count")
        t_alt_count_idx = header.index("t_alt_count")
        # ref_allele_idx = header.index("Reference_Allele")
        # alt1_allele_idx = header.index("Tumor_Seq_Allele1")
        # alt2_allele_idx = header.index("Tumor_Seq_Allele2")
        sample_idx = header.index("Tumor_Sample_Barcode")
        gene_idx = header.index("Hugo_Symbol")
        tot_n = 0
        selected_n = 0
        for line in input_snv:
            tot_n += 1
            line = line.strip().split("\t")
            is_snv = True
            var_type = line[var_type_idx]
            if var_type != "SNP":
                is_snv = False
            if not is_snv:
                continue
            sample_id = line[sample_idx]
            consequence = line[var_class_idx]
            try:
                cellularity = sample_donor.loc[sample_id, "cellularity"]
            except KeyError:
                logger.error(
                    "Could not retrive cellularity estimate for sample %s"
                    % sample_id
                )
                continue
            vaf = vaf_from_maf(line[t_ref_count_idx], line[t_alt_count_idx])
            # ref_allele = line[var_type_idx]
            # alt1_allele = line[alt1_allele_idx]
            # alt2_allele = line[alt2_allele_idx]
            try:
                scaled_vaf = vaf / cellularity
            except ZeroDivisionError:
                scaled_vaf = 0
            if consequence in damaging_mutations and is_snv:
                selected_n += 1
                aberration_data.append(
                    Aberration(
                        **{
                            "chrom": str(line[chromosome_idx]),
                            "start": int(line[start_idx]),
                            "end": int(line[end_idx]),
                            "aberration_type": AberrationType.SNV,
                            "aberration_subtype": consequence,
                            "sample_id": sample_id,
                            "gene": line[gene_idx],
                            "vaf": scaled_vaf,
                            "id": ".",
                        }
                    )
                )
        logger.info("Selected %i SNVs over %i variants" % (selected_n, tot_n))
        return pd.DataFrame(vars(a) for a in aberration_data)


def indel(file_name, logger, annotation, min_scaled_vaf=0.05):
    aberration_data = []
    damaging_mutations = [
        "Frame_Shift_Del",
        "Frame_Shift_Ins",
        "In_Frame_Del",
        "In_Frame_Ins",
        "Missense_Mutation",
        "Nonsense_Mutation",
        "Splice_Site",
        "Translation_Start_Site",
        "Nonstop_Mutation",
    ]
    logger.info("Read file %s as MAF format" % file_name)
    sample_donor = annotation["sample_donors"].set_index("sample_id")
    with xopen(file_name, "rt") as input_snv:
        header = next(input_snv).strip().split("\t")
        chromosome_idx = header.index("Chromosome")
        start_idx = header.index("Start_Position")
        end_idx = header.index("End_Position")
        gene_idx = header.index("Hugo_Symbol")
        var_class_idx = header.index("Variant_Classification")
        var_type_idx = header.index("Variant_Type")
        t_ref_count_idx = header.index("t_ref_count")
        t_alt_count_idx = header.index("t_alt_count")
        # ref_allele_idx = header.index("Reference_Allele")
        # alt1_allele_idx = header.index("Tumor_Seq_Allele1")
        # alt2_allele_idx = header.index("Tumor_Seq_Allele2")
        sample_idx = header.index("Tumor_Sample_Barcode")
        sample_donor = annotation["sample_donors"].set_index("sample_id")
        tot_n = 0
        selected_n = 0
        for line in input_snv:
            tot_n += 1
            line = line.strip().split("\t")
            is_indel = False
            var_type = line[var_type_idx]
            if var_type == "INS" or var_type == "DEL":
                is_indel = True
            if not is_indel:
                continue
            sample_id = line[sample_idx]
            consequence = line[var_class_idx]

            try:
                cellularity = sample_donor.loc[sample_id, "cellularity"]
            except KeyError:
                logger.error(
                    "Could not retrive cellularity estimate for sample %s"
                    % sample_id
                )
                continue

            vaf = vaf_from_maf(line[t_ref_count_idx], line[t_alt_count_idx])
            try:
                scaled_vaf = vaf / cellularity
            except ZeroDivisionError:
                scaled_vaf = 0
            # ref_allele = line[var_type_idx]
            # alt1_allele = line[alt1_allele_idx]
            # alt2_allele = line[alt2_allele_idx]
            if consequence in damaging_mutations and is_indel:
                selected_n += 1
                aberration_data.append(
                    Aberration(
                        **{
                            "chrom": str(line[chromosome_idx]),
                            "start": int(line[start_idx]),
                            "end": int(line[end_idx]),
                            "aberration_type": AberrationType.INDEL,
                            "aberration_subtype": consequence,
                            "sample_id": sample_id,
                            "gene": line[gene_idx],
                            "vaf": scaled_vaf,
                            "id": ".",
                        }
                    )
                )
        logger.info(
            "Selected %i INDELs over %i variants" % (selected_n, tot_n)
        )
        return pd.DataFrame(vars(a) for a in aberration_data)


def germ_snv(file_name, logger, annotation, max_af=1):
    aberration_data = []
    damaging_mutations = [
        "Frame_Shift_Del",
        "Frame_Shift_Ins",
        "In_Frame_Del",
        "In_Frame_Ins",
        "Missense_Mutation",
        "Nonsense_Mutation",
        "Splice_Site",
        "Translation_Start_Site",
        "Nonstop_Mutation",
    ]
    logger.info("Read file %s as MAF format" % file_name)
    with xopen(file_name, "rt") as input_snv:
        header = next(input_snv).strip().split("\t")
        chromosome_idx = header.index("Chromosome")
        start_idx = header.index("Start_Position")
        end_idx = header.index("End_Position")
        gene_idx = header.index("Hugo_Symbol")
        var_class_idx = header.index("Variant_Classification")
        var_type_idx = header.index("Variant_Type")
        t_ref_count_idx = header.index("t_ref_count")
        t_alt_count_idx = header.index("t_alt_count")
        af_idx = header.index("gnomAD_AF")
        # ref_allele_idx = header.index("Reference_Allele")
        # alt1_allele_idx = header.index("Tumor_Seq_Allele1")
        # alt2_allele_idx = header.index("Tumor_Seq_Allele2")
        sample_idx = header.index("Tumor_Sample_Barcode")

        tot_n = 0
        selected_n = 0
        for line in input_snv:
            tot_n += 1
            line = line.strip().split("\t")
            consequence = line[var_class_idx]
            is_snv = True
            vaf = vaf_from_maf(line[t_ref_count_idx], line[t_alt_count_idx])
            var_type = line[var_type_idx]
            try:
                genomad_af = float(line[af_idx])
            except ValueError:
                genomad_af = 0
            # ref_allele = line[var_type_idx]
            # alt1_allele = line[alt1_allele_idx]
            # alt2_allele = line[alt2_allele_idx]
            if consequence in damaging_mutations and genomad_af <= max_af:
                selected_n += 1
                aberration_data.append(
                    Aberration(
                        **{
                            "chrom": str(line[chromosome_idx]),
                            "start": int(line[start_idx]),
                            "end": int(line[end_idx]),
                            "aberration_type": AberrationType.GERM_SNV,
                            "aberration_subtype": consequence,
                            "sample_id": line[sample_idx],
                            "gene": line[gene_idx],
                            "vaf": vaf,
                            "id": ".",
                        }
                    )
                )
        logger.info("Selected %i GERM_SNVs over %i variants" % (selected_n, tot_n))
        return pd.DataFrame(vars(a) for a in aberration_data)


def vaf_from_maf(n_ref, n_alt):
    tot = int(round(float(n_ref), 0)) + int(round(float(n_alt), 0))
    try:
        return float(n_alt) / tot
    except ZeroDivisionError:
        return 0
