from biallelic.misc import xopen
from biallelic.models import Aberration, AberrationType
import pandas as pd


annot_header = [
    "chr",
    "pos1",
    "ref",
    "alt",
    "PPCG_Sample_ID",
    "qual",
    "filter",
    "VAF",
    "callers",
    "is_conservative",
    "Func.refGene",
    "Gene.refGene",
    "GeneDetail.refGene",
    "ExonicFunc.refGene",
    "AAChange.refGene",
    "cytoBand",
    "gnomAD_genome_ALL",
    "avsnp150",
]

exonic_func = [
    "nonsynonymous  SNV",
    "startloss",
    "stopgain",
    "stoploss",
    "synonymous SNV",
    "unknown",
]

indels_exonic_func = [
    "frameshift substitution",
    "nonframeshift substitution",
    "nonsynonymous SNV",
    "startloss",
    "stopgain",
    "stoploss",
    "synonymous SNV",
    "unknown",
]


def snv(file_name, logger, annotation, ploidy_file, min_scaled_vaf=0.05):
    ploidy_dict = {}
    aberration_data = []
    logger.info(
        "Collect ploidy and cellularity information from %s" % ploidy_file
    )
    with xopen(ploidy_file, "rt") as ploidy_info:
        header = next(ploidy_info).strip().split(",")
        sample_idx = header.index("PPCG_Sample_ID")
        #sample_idx = header.index("tumor_id")
        cellularity_idx = header.index("Cellularity")
        ploidy_idx = header.index("ploidy")
        for sample_line in ploidy_info:
            sample_line = sample_line.strip().split(",")
            ploidy_dict[sample_line[sample_idx]] = {
                "ploidy": float(sample_line[ploidy_idx]),
                "cellularity": float(sample_line[cellularity_idx]),
            }
    damaging_mutations = [
        "nonsynonymous SNV",
        "startloss",
        "stopgain",
        "stoploss",
    ]
    logger.info("Read file %s as TSV format" % file_name)
    with xopen(file_name, "rt") as input_snv:
        header = next(input_snv).strip().split("\t")
        chromosome_idx = header.index("chr")
        start_idx = header.index("pos1")
        var_class_idx = header.index("ExonicFunc.refGene")
        vaf_idx = header.index("VAF")
        #is_conservative_idx = header.index("is_conservative")
        #sample_idx = header.index("PPCG_Sample_ID")
        sample_idx = header.index("tumor_id")
        tot_n = 0
        selected_n = 0
        for line in input_snv:
            tot_n += 1
            line = line.strip().split("\t")
            consequence = line[var_class_idx]
            sample_id = line[sample_idx]
            #is_conservative = line[is_conservative_idx] == "TRUE"
            is_conservative = True
            try:
                vaf = float(line[vaf_idx])
            except ValueError:
                logger.error("Could not convert VAF information for %s" % line)
                continue
            try:
                cellularity = ploidy_dict[sample_id]["cellularity"]
            except KeyError:
                logger.error(
                    "Could not retrive cellularity estimate for sample %s"
                    % sample_id
                )
                continue
            try:
                scaled_vaf = vaf / cellularity
            except ZeroDivisionError:
                scaled_vaf = 0
            if is_conservative and consequence in damaging_mutations:
                selected_n += 1
                aberration_data.append(
                    Aberration(
                        **{
                            "chrom": str(line[chromosome_idx]).replace(
                                "chr", "", 1
                            ),
                            "start": int(line[start_idx]),
                            "end": int(line[start_idx]) + 1,
                            "aberration_type": AberrationType.SNV,
                            "aberration_subtype": consequence,
                            "sample_id": sample_id,
                            "vaf": scaled_vaf,
                        }
                    )
                )
        logger.info("Selected %i SNVs over %i variants" % (selected_n, tot_n))
        return pd.DataFrame(vars(a) for a in aberration_data)


def indel(file_name, logger, annotation, ploidy_file, min_scaled_vaf=0.9):
    ploidy_dict = {}
    aberration_data = []
    logger.info(
        "Collect ploidy and cellularity information from %s" % ploidy_file
    )
    with xopen(ploidy_file, "rt") as ploidy_info:
        header = next(ploidy_info).strip().split(",")
        sample_idx = header.index("PPCG_Sample_ID")
        #sample_idx = header.index("tumor_id")
        cellularity_idx = header.index("Cellularity")
        ploidy_idx = header.index("ploidy")
        for sample_line in ploidy_info:
            sample_line = sample_line.strip().split(",")
            ploidy_dict[sample_line[sample_idx]] = {
                "ploidy": float(sample_line[ploidy_idx]),
                "cellularity": float(sample_line[cellularity_idx]),
            }
    damaging_mutations = [
        "frameshift substitution",
        "nonsynonymous SNV",
        "startloss",
        "stopgain",
        "stoploss",
    ]
    logger.info("Read file %s as TSV format" % file_name)
    with xopen(file_name, "rt") as input_snv:
        header = next(input_snv).strip().split("\t")
        chromosome_idx = header.index("chr")
        start_idx = header.index("pos1")
        end_idx = header.index("pos2")
        var_class_idx = header.index("ExonicFunc.refGene")
        vaf_idx = header.index("VAF")
        #sample_idx = header.index("PPCG_Sample_ID")
        sample_idx = header.index("tumor_id")
        callers_idx = header.index("callers")
        gnomAD_idx = header.index("gnomAD_genome_ALL")
        tot_n = 0
        selected_n = 0
        for line in input_snv:
            tot_n += 1
            line = line.strip().split("\t")
            consequence = line[var_class_idx]
            sample_id = line[sample_idx]
            is_conservative = line[callers_idx] == "pindel__platypus"
            try:
                vaf = float(line[vaf_idx])
            except ValueError:
                logger.error("Could not convert VAF information for %s" % line)
                vaf = 0
                # continue
            try:
                af = float(line[gnomAD_idx])
            except ValueError:
                af = 0
            try:
                cellularity = ploidy_dict[sample_id]["cellularity"]
            except KeyError:
                logger.error(
                    "Could not retrive cellularity estimate for sample %s"
                    % sample_id
                )
                continue
            try:
                scaled_vaf = vaf / cellularity
            except ZeroDivisionError:
                scaled_vaf = 0
            if (
                af < 0.01
                and is_conservative
                and consequence in damaging_mutations
            ):
                selected_n += 1
                aberration_data.append(
                    Aberration(
                        **{
                            "chrom": str(line[chromosome_idx]).replace(
                                "chr", "", 1
                            ),
                            "start": int(line[start_idx]),
                            "end": int(line[start_idx]) + 1,
                            "aberration_type": AberrationType.INDEL,
                            "aberration_subtype": consequence,
                            "sample_id": sample_id,
                            "vaf": scaled_vaf,
                        }
                    )
                )
        logger.info("Selected %i SNVs over %i variants" % (selected_n, tot_n))
        return pd.DataFrame(vars(a) for a in aberration_data)
