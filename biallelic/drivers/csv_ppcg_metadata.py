from biallelic.misc import xopen
from biallelic.models import SampleDonor, Gender
import pandas as pd


def sample_donors(file_path, logger):
    donors_info = []
    logger.info("Read Sample/Donor information from %s" % file_path)
    with xopen(file_path, "rt") as ppcg_meta:
        header = next(ppcg_meta).strip().split(",")
        sample_id_idx = header.index("PPCG_Sample_ID")
        donor_id_idx = header.index("PPCG_Donor_ID")
        for line in ppcg_meta:
            line = line.strip().split(",")
            donors_info.append(
                SampleDonor(
                    line[sample_id_idx], line[donor_id_idx], Gender.Male
                )
            )
    logger.info("Recorded %i Sample/Donor information" % len(donors_info))
    return pd.DataFrame(vars(d) for d in donors_info)
