from biallelic.misc import xopen
from biallelic.models import Aberration, AberrationType
import pandas as pd


# header of the file
# ID      chrom   loc.start       loc.end num.mark        seg.mean
def scna(file_name, logger, annotation):
    aberration_data = []
    logger.info("Read Segments file from %s" % file_name)
    with xopen(file_name, "rt") as input_scna:
        header = next(input_scna).strip().split("\t")
        sample_idx = 0
        chromosome_idx = 1
        start_idx = 2
        end_idx = 3
        n_probes = 4
        logr_idx = 5
        tot_n = 0
        selected_n = 0
        for line in input_scna:
            tot_n += 1
            line = line.strip().split("\t")
            logr = float(line[logr_idx])
            n_copy = 2
            if logr < -0.2:
                aberration_type = AberrationType.HET_LOSS
                n_copy = 1
                if logr < -1:
                    aberration_type = AberrationType.HOM_LOSS
                    n_copy = 0
                selected_n += 1
                chromosome = str(line[chromosome_idx])
                if chromosome.endswith("23"):
                    chromosome = chromosome.replace("23", "X")
                elif chromosome.endswith("24"):
                    chromosome = chromosome.replace("24", "Y")
                aberration_data.append(
                    Aberration(
                        **{
                            "chrom": chromosome,
                            "start": int(line[start_idx]),
                            "end": int(line[end_idx]),
                            "aberration_type": aberration_type,
                            "aberration_subtype": "DEL",
                            "sample_id": line[sample_idx],
                            "n_copy": n_copy,
                            "vaf": 1,
                            "gene": ".",
                            "id": ".",
                        }
                    )
                )
        logger.info("Selected %i segments over %i" % (selected_n, tot_n))
        return pd.DataFrame(vars(a) for a in aberration_data)
