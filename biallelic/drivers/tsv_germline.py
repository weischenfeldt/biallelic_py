from biallelic.misc import xopen
from biallelic.models import Aberration, AberrationType
import pandas as pd



def germ_snv(file_name, logger, annotation, metadata):
    ppcg_ids_map = collect_ppcg_samples_ids(metadata, logger)
    aberration_data = []
    n = 0
    pn = 0
    t = 0
    # dtypes={"chrom":str, "start":int, "end":int}
    # return pd.read_csv("../bi_ppcg/results/aberration_0.tsv", sep="\t").astype(dtypes)
    with xopen(file_name, "rt") as germline_snv:
        header = next(germline_snv).strip().split('\t')
        donor_idx = header.index('Sample')
        gene_idx = header.index('Gene')
        chr_idx = header.index('Chr')
        pos_idx = header.index('Position')
        ref_idx = header.index('REF')
        alt_idx = header.index('ALT')
        type_idx = header.index('Type')
        cons_idx = header.index('Consequence')
        aberration_type = AberrationType.GERM_SNV

        for rec in germline_snv:
            rec_line = rec.strip().split('\t')
            start_pos = int(rec_line[pos_idx])
            end_pos = start_pos + len(rec_line[alt_idx])
            t += 1
            cons = rec_line[cons_idx]
            try:
                sample_id = ppcg_ids_map[ rec_line[donor_idx]]
                aberration_data.append(
                    Aberration(
                        **{
                            "chrom": rec_line[chr_idx],
                            "start": start_pos,
                            "end": end_pos,
                            "aberration_type": aberration_type,
                            "aberration_subtype": rec_line[cons_idx],
                            "gene": rec_line[gene_idx],
                            "sample_id": sample_id,
                        }
                    )
                )
            except KeyError:
                pass
    logger.info(
        "Parsed %i germline variants" % t
    )
    return pd.DataFrame(vars(a) for a in aberration_data)


def collect_ppcg_samples_ids(metadata, logger):
    logger.info("Open PPCG metadata %s" % metadata)
    ppcg_donor_to_sample = {}
    with xopen(metadata, "rt") as ppcg_info:
        header = next(ppcg_info).strip().split(",")
        ppcg_id_idx = header.index("PPCG_Sample_ID")
        ppcg_donor_id_idx = header.index("PPCG_Donor_ID")
        is_normal_idx = header.index("Is_Normal")
        for line in ppcg_info:
            line = line.strip().split(",")
            is_normal = line[is_normal_idx] == "Y"
            if is_normal:
                ppcg_donor_to_sample[line[ppcg_donor_id_idx]] = line[ppcg_id_idx]
    logger.info(
        "Recorder %i sample information" % len(ppcg_donor_to_sample)
    )
    return ppcg_donor_to_sample
