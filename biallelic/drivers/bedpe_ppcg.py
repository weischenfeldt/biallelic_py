from biallelic.misc import xopen
from biallelic.models import Aberration, AberrationType
import pandas as pd
import os


HEADERS = [
    "chrom1",
    "start1",
    "end1",
    "chrom2",
    "start2",
    "end2",
    "sv_id",
    "pe_support",
    "strand1",
    "strand2",
    "svclass",
    "svmethod",
    "samples",
]

FILE_ENDS_WITH = "consensus.somatic.sv.bedpe"


def sv(file_name, logger, annotation):
    ploidy_dict = {}
    aberration_data = []
    if os.path.isdir(file_name):
        sv_files = [
            f for f in os.listdir(file_name) if f.endswith(FILE_ENDS_WITH)
        ]
        logger.info(
            "Parse SV information in BEDPE format from %s files"
            % len(sv_files)
        )
        for sv_file in sv_files:
            sv_file_path = os.path.join(file_name, sv_file)
            aberration_data += parse_sv_calls(sv_file_path, logger)
        logger.info("Found %i aberrant SVs" % len(aberration_data))
        return pd.DataFrame(vars(a) for a in aberration_data)


def parse_sv_calls(file_name, logger):
    aberration_data = []
    with xopen(file_name, "rt") as sv_calls:
        try:
            header = next(sv_calls).strip().split("\t")
        except StopIteration:
            return aberration_data
        chrom1_idx = header.index("chrom1")
        start1_idx = header.index("start1")
        end1_idx = header.index("end1")
        chrom2_idx = header.index("chrom2")
        start2_idx = header.index("start2")
        end2_idx = header.index("end2")
        svclass_idx = header.index("svclass")
        sv_id_idx = header.index("sv_id")
        samples_idx = header.index("samples")
        for sv in sv_calls:
            sv = sv.strip().split("\t")
            samples = sv[samples_idx].split(" ")
            for sample_id in samples:
                if sv[svclass_idx] == "TRA":
                    aberration_data.append(
                        Aberration(
                            **{
                                "chrom": sv[chrom1_idx],
                                "start": int(sv[start1_idx]),
                                "end": int(sv[end1_idx]),
                                "aberration_type": AberrationType.SV,
                                "aberration_subtype": sv[svclass_idx],
                                "sample_id": sample_id,
                                "gene": ".",
                                "id": sv[sv_id_idx],
                            }
                        )
                    )
                    aberration_data.append(
                        Aberration(
                            **{
                                "chrom": sv[chrom2_idx],
                                "start": int(sv[start2_idx]),
                                "end": int(sv[end2_idx]),
                                "aberration_type": AberrationType.SV,
                                "aberration_subtype": sv[svclass_idx],
                                "sample_id": sample_id,
                                "gene": ".",
                                "id": sv[sv_id_idx],
                            }
                        )
                    )
                else:
                    aberration_data.append(
                        Aberration(
                            **{
                                "chrom": sv[chrom2_idx],
                                "start": int(sv[start1_idx]),
                                "end": int(sv[end2_idx]),
                                "aberration_type": AberrationType.SV,
                                "aberration_subtype": sv[svclass_idx],
                                "sample_id": sample_id,
                                "gene": ".",
                                "id": sv[sv_id_idx],
                            }
                        )
                    )
    return aberration_data
