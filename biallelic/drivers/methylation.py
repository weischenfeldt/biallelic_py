import numpy as np
import pandas as pd
#import seaborn as sns
#import warnings
#import statistics as st
#import matplotlib.pyplot as plt
#from scipy.stats import gaussian_kde
#from sklearn.mixture import GaussianMixture
#from scipy import signal
#from enum import Enum
from biallelic.misc import xopen
from biallelic.models import (
    Aberration,
    AberrationType,
    OmicsType,
    SampleDonor,
    Gender,
)

def categorize_meth(meth_series, down_threshold=.2, up_threshold=.7, plot=False,
                    quant_low=0.3, quant_high=0.7):
    """
    :meth_series: pandas.Series for one metylation feature (make sure that use only TUM/MET samples)
    :down_threshold: float, annotate features with low methylation range as 'Not/Low_methyl'
    :up_threshold: float, annotate features with high methylation range as 'Highly_methyl'
    :plot: boolean, if True, will plot Seaborn.distplot with three methylation groups for one feature
    :quant_low: float, methylation values < quant_low will be considered as Not/Low_methyl
    :quant_high: float, methylation values > quant_high will be considered as Highly methyl

    :return: pandas.Series with 3 categories ['Not/Low_methyl', 'Medium_methyl', 'Highly_methyl']
    for each feature
    """
    feature = meth_series.name
    meth_series = meth_series[meth_series.notna()]
    meth_series = meth_series.astype(float)
    feature_values_i = pd.DataFrame(meth_series)
    feature_values_i['value'] = feature_values_i[feature]

    # annotate features with low methylation range [0:0.2]
    if feature_values_i[feature].max() < down_threshold:

        feature_values_i[feature] = 'Not/Low_methyl'

        if plot:
            sns.distplot(feature_values_i['value'],
                         hist=False, rug=True, color='#FFD3D3')
            plt.legend(labels=['Not/Low_methyl'])
            plt.xlabel(f'{feature} methylation')
            plt.xlim(0, 1)

        categorised = feature_values_i[feature]

    # annotate features with low methylation range [0.7:]
    elif feature_values_i[feature].min() >= up_threshold:

        feature_values_i[feature] = 'Highly_methyl'

        if plot:
            sns.distplot(feature_values_i['value'],
                         hist=False, rug=True, color='#D50000')
            plt.legend(labels=['Highly_methyl'])
            plt.xlabel(f'{feature} methylation')
            plt.xlim(0, 1)

        categorised = feature_values_i[feature]

    else:
        kde = gaussian_kde(feature_values_i[feature])
        x = np.linspace(feature_values_i[feature].min(),
                        feature_values_i[feature].max(), 1000)
        pdf = kde(x)

        # Find the local maxima of the estimated density
        maxima, _ = signal.find_peaks(pdf, height=1.1)

        # Identify the number and location of the peaks
        if len(maxima) == 1:
            peak_vals = pd.Series([x[maxima[0]]], index=['peak_1'])
        elif len(maxima) == 2:
            if pdf[maxima[0]] >= pdf[maxima[1]]:
                peak_vals = pd.Series([x[maxima[0]], x[maxima[1]]], index=['peak_1', 'peak_2'])
            else:
                peak_vals = pd.Series([x[maxima[1]], x[maxima[0]]], index=['peak_1', 'peak_2'])
        else:
            peak_vals = pd.Series([x[maxima[0]], x[maxima[1]], x[maxima[2]]], index=['peak_1', 'peak_2', 'peak_3'])

        # Save the peak values to a separate dataframe if the peak height is above a certain threshold
        peak_df = pd.DataFrame()
        for peak in peak_vals.index:
            if pdf[int(np.round((peak_vals[peak] - feature_values_i[feature].min()) / (
                    feature_values_i[feature].max() - feature_values_i[feature].min()) * 999))] > 1.1:
                peak_df[peak] = pd.Series([peak_vals[peak]])
        peak_df.index = [feature]

        # 1 peak
        if len(peak_df.columns) == 1:

            gmm = GaussianMixture(n_components=3, covariance_type='full')
            gmm.fit(meth_series.to_numpy().reshape(-1, 1))
            labels = gmm.predict(meth_series.to_numpy().reshape(-1, 1))

            feature_values_i['labels'] = labels

            if len(feature_values_i['labels'].unique()) == 3:

                cl0 = st.mode(feature_values_i[feature][feature_values_i.labels == 0])
                cl1 = st.mode(feature_values_i[feature][feature_values_i.labels == 1])
                cl2 = st.mode(feature_values_i[feature][feature_values_i.labels == 2])

                up_cl_threshold = np.median([cl0, cl1, cl2])
                down_cl_threshold = np.min([cl0, cl1, cl2])

                feature_values_i[feature] = np.where(feature_values_i[feature] < down_cl_threshold,
                                                     'Not/Low_methyl',
                                                     np.where(feature_values_i[feature] > up_cl_threshold,
                                                              'Highly_methyl', 'Medium_methyl'))
                categorised = feature_values_i[feature]

                if plot:
                    sns.distplot(feature_values_i[feature_values_i[feature] == 'Not/Low_methyl']['value'],
                                 hist=False, rug=True, color='#FFD3D3', kde_kws={'linestyle': '-'})
                    sns.distplot(feature_values_i[feature_values_i[feature] == 'Medium_methyl']['value'],
                                 hist=False, rug=True, color='#FF8787', kde_kws={'linestyle': '-'})
                    sns.distplot(feature_values_i[feature_values_i[feature] == 'Highly_methyl']['value'],
                                 hist=False, rug=True, color='#D50000', kde_kws={'linestyle': '-'})
                    plt.legend(labels=['Not/Low_methyl', 'Medium_methyl', 'Highly_methyl'])
                    plt.axvline(x=up_cl_threshold, color='black', linestyle=':')
                    plt.axvline(x=down_cl_threshold, color='black', linestyle=':')
                    plt.xlabel(f'{feature} methylation GMM')
                    plt.xlim(0, 1)
                else:
                    pass

            else:
                # for cases when GMM didn't work
                q_low = feature_values_i[feature].quantile(quant_low)
                q_high = feature_values_i[feature].quantile(quant_high)

                feature_values_i[feature] = np.where(feature_values_i[feature] < q_low,
                                                     'Not/Low_methyl',
                                                     np.where(feature_values_i[feature] > q_high,
                                                              'Highly_methyl', 'Medium_methyl'))
                categorised = feature_values_i[feature]

                if plot:
                    sns.distplot(feature_values_i[feature_values_i[feature] == 'Not/Low_methyl']['value'],
                                 hist=False, rug=True, color='#FFD3D3', kde_kws={'linestyle': '-'})
                    sns.distplot(feature_values_i[feature_values_i[feature] == 'Medium_methyl']['value'],
                                 hist=False, rug=True, color='#FF8787', kde_kws={'linestyle': '-'})
                    sns.distplot(feature_values_i[feature_values_i[feature] == 'Highly_methyl']['value'],
                                 hist=False, rug=True, color='#D50000', kde_kws={'linestyle': '-'})
                    plt.legend(labels=['Not/Low_methyl', 'Medium_methyl', 'Highly_methyl'])
                    plt.axvline(x=q_high, color='black', linestyle=':')
                    plt.axvline(x=q_low, color='black', linestyle=':')
                    plt.xlabel(f'{feature} methylation quantiles')
                    plt.xlim(0, 1)
                else:
                    pass


        # 2 peaks
        elif len(peak_df.columns) == 2:

            warnings.warn(f'This feature {feature} has bimodal distribution you may want to review it manually')

            up_cl_threshold = peak_df.loc[feature].max()
            down_cl_threshold = peak_df.loc[feature].min()

            feature_values_i[feature] = np.where(feature_values_i[feature] < down_cl_threshold,
                                                 'Not/Low_methyl',
                                                 np.where(feature_values_i[feature] >= up_cl_threshold,
                                                          'Highly_methyl', 'Medium_methyl'))
            if plot:
                sns.distplot(feature_values_i[feature_values_i[feature] == 'Not/Low_methyl']['value'],
                             hist=False, rug=True, color='#FFD3D3', kde_kws={'linestyle': '-'})
                sns.distplot(feature_values_i[feature_values_i[feature] == 'Medium_methyl']['value'],
                             hist=False, rug=True, color='#FF8787', kde_kws={'linestyle': '-'})
                sns.distplot(feature_values_i[feature_values_i[feature] == 'Highly_methyl']['value'],
                             hist=False, rug=True, color='#D50000', kde_kws={'linestyle': '-'})
                plt.legend(labels=['Not/Low_methyl', 'Medium_methyl', 'Highly_methyl'])
                plt.axvline(x=up_cl_threshold, color='black', linestyle=':')
                plt.axvline(x=down_cl_threshold, color='black', linestyle=':')
                plt.xlabel(f'{feature} methylation KDE+finder')
                plt.xlim(0, 1)
            else:
                pass

            categorised = feature_values_i[feature]

        # 3 peaks
        elif len(peak_df.columns) == 3:

            warnings.warn(f'This feature {feature} has multimodal distribution you may want to review it manually')

            up_cl_threshold = peak_df.loc[feature].median()
            down_cl_threshold = peak_df.loc[feature].min()

            feature_values_i[feature] = np.where(feature_values_i[feature] < down_cl_threshold,
                                                 'Not/Low_methyl',
                                                 np.where(feature_values_i[feature] > up_cl_threshold,
                                                          'Highly_methyl', 'Medium_methyl'))
            if plot:
                sns.distplot(feature_values_i[feature_values_i[feature] == 'Not/Low_methyl']['value'],
                             hist=False, rug=True, color='#FFD3D3', kde_kws={'linestyle': '-'})
                sns.distplot(feature_values_i[feature_values_i[feature] == 'Medium_methyl']['value'],
                             hist=False, rug=True, color='#FF8787', kde_kws={'linestyle': '-'})
                sns.distplot(feature_values_i[feature_values_i[feature] == 'Highly_methyl']['value'],
                             hist=False, rug=True, color='#D50000', kde_kws={'linestyle': '-'})
                plt.legend(labels=['Not/Low_methyl', 'Medium_methyl', 'Highly_methyl'])
                plt.axvline(x=up_cl_threshold, color='black', linestyle=':')
                plt.axvline(x=down_cl_threshold, color='black', linestyle=':')
                plt.xlabel(f'{feature} methylation KDE+finder')
                plt.xlim(0, 1)
            else:
                pass

            categorised = feature_values_i[feature]

    return categorised

def methylation(df, path=False, sep='\t', header=0, index_col=0, comment=None, **kwargs):
    """
    Load methylation data and process it
    :param: df: str, path to methylation data beta values for all samples if path=True or
                pandas.DataFrame if path=False;
                rows - features, columns - Chromosome, Start, End and the rest are Sample names
    :param path: boolean, if True, will read data from the path to df, if False, will take df variable
    :param sep: str, delimiter to use, passed to pandas.read_csv
    :param header: int, row number(s) to use as the column names, passed to pandas.read_csv
    :param index_col: int, str column to use as row name, passed to pandas.read_csv
    :param comment: str, indicates line's index that should not be parsed, passed to pandas.read_csv

    :return: pandas.DataFrame with 3 categories ['Not/Low_methyl', 'Medium methyl', 'Highly methyl']
    for each feature
    """
    if path:
        beta_values = pd.read_csv(df, sep=sep, index_col=index_col)
    else:
        beta_values = df

    regions = beta_values[['Chromosome', 'Start', 'End']]

    beta_values = beta_values.loc[:, ~beta_values.columns.isin(['Chromosome', 'Start', 'End'])].astype(float)

    processed_beta_val = beta_values.apply(lambda x: categorize_meth(x, **kwargs), axis=1)

    processed_beta_val = pd.concat([regions, processed_beta_val], axis=1)

    return processed_beta_val


def meth(file_name, logger, annotation):
    aberration_data = []
    logger.info("Read file %s as Methylation format" % file_name)
    with xopen(file_name, "rt") as input_meth:
        header = next(input_meth).rstrip().split("\t")
        id_idx = 0
        chromosome_idx = header.index("Chromosome")
        start_idx = header.index("Start")
        end_idx = header.index("End")
        sample_colum_offset = 3
        samples_names = header[sample_colum_offset:]
        tot_n = 0
        selected_n = 0
        for line in input_meth:
            tot_n += 1
            line = line.strip().split("\t")
            for sample_index in range(len(samples_names) - 1):
                samople_name = samples_names[sample_index]
                meth_status = line[sample_index + sample_colum_offset]
                if meth_status == "Highly_methyl":
                    selected_n += 1
                    aberration_data.append(
                        Aberration(
                            **{
                                "chrom": str(line[chromosome_idx]).replace("chr", ""),
                                "start": int(line[start_idx]),
                                "end": int(line[end_idx]),
                                "aberration_type": AberrationType.METHYL,
                                "aberration_subtype": "Highly_methyl",
                                "sample_id": samople_name,
                                "gene": ".",
                                "vaf": 1,
                                "id": str(line[id_idx]),
                            }
                        )
                    )
        logger.info(
            "Selected %i methylations sites over %i probes in %i samples" % (selected_n, tot_n, len(samples_names))
        )
        return pd.DataFrame(vars(a) for a in aberration_data)
