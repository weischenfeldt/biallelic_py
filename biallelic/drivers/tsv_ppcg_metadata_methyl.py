from biallelic.misc import xopen
from biallelic.models import SampleDonor, Gender, OmicsType
import pandas as pd


#        sample_id: str,
#        donor_id: str,
#        gender: Gender,
#        omics: OmicsType = OmicsType.Genomics,
#        cellularity: float = 0,
#        ploidy: float = 2,
#        matching_sample_id: str = "",


# Country PPCG_Donor_ID Matching_WGS_Sample WGS_Local_Sample_ID
# PPCG_Meth_Assay_ID PPCG_RNA_Assay_ID Platform RNA_Local_Assay_ID
# RNA_Sample_Type MetastaticSite RNA_Distance_To_WGS_Sample Meth_RNAseq_annott
# fastq_filename bam_filename Date_of_scan_or_Sequencing Comments Library Preparation FFPE Batch/Study

def sample_donors(file_path, logger):
    donors_info = []
    logger.info("Read Sample/Donor information from %s" % file_path)
    with xopen(file_path, "rt") as ppcg_meta:
        header = next(ppcg_meta).strip("\n").split("\t")
        sample_id_idx = header.index("Matching_WGS_Sample")
        donor_id_idx = header.index("PPCG_Donor_ID")
        rna_sample_id_idx = header.index("PPCG_RNA_Assay_ID")
        meth_sample_id_idx = header.index("PPCG_Meth_Assay_ID")

        for line in ppcg_meta:
            line = line.strip("\n").split("\t")
            if line[sample_id_idx] != "NA":
                donors_info.append(
                    SampleDonor(
                        line[sample_id_idx], line[donor_id_idx], Gender.Male
                    )
                )
            if line[meth_sample_id_idx] != "NA":
                donors_info.append(
                    SampleDonor(
                        line[meth_sample_id_idx], line[donor_id_idx], Gender.Male, OmicsType.Methylomics, matching_sample_id = line[sample_id_idx]
                    )
                )
            if line[rna_sample_id_idx] != "NA":
                donors_info.append(
                    SampleDonor(
                        line[rna_sample_id_idx], line[donor_id_idx], Gender.Male, OmicsType.Transcriptomics, matching_sample_id = line[sample_id_idx]
                    )
                )
            
    logger.info("Recorded %i Sample/Donor information" % len(donors_info))
    return pd.DataFrame(vars(d) for d in donors_info)
