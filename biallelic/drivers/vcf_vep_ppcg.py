from pysam import VariantFile, VariantHeader, VariantRecord
from biallelic.misc import xopen
from biallelic.models import Aberration, AberrationType
import pandas as pd


VEP_headers = [
    "Allele",
    "Consequence",
    "IMPACT",
    "SYMBOL",
    "Gene",
    "Feature_type",
    "Feature",
    "BIOTYPE",
    "EXON",
    "INTRON",
    "HGVSc",
    "HGVSp",
    "cDNA_position",
    "CDS_position",
    "Protein_position",
    "Amino_acids",
    "Codons",
    "Existing_variation",
    "DISTANCE",
    "STRAND",
    "FLAGS",
    "VARIANT_CLASS",
    "SYMBOL_SOURCE",
    "HGNC_ID",
    "CANONICAL",
    "TSL",
    "APPRIS",
    "CCDS",
    "ENSP",
    "SWISSPROT",
    "TREMBL",
    "UNIPARC",
    "SOURCE",
    "GENE_PHENO",
    "SIFT",
    "PolyPhen",
    "DOMAINS",
    "HGVS_OFFSET",
    "AF",
    "AFR_AF",
    "AMR_AF",
    "EAS_AF",
    "EUR_AF",
    "SAS_AF",
    "AA_AF",
    "EA_AF",
    "gnomAD_AF",
    "gnomAD_AFR_AF",
    "gnomAD_AMR_AF",
    "gnomAD_ASJ_AF",
    "gnomAD_EAS_AF",
    "gnomAD_FIN_AF",
    "gnomAD_NFE_AF",
    "gnomAD_OTH_AF",
    "gnomAD_SAS_AF",
    "MAX_AF",
    "MAX_AF_POPS",
    "CLIN_SIG",
    "SOMATIC",
    "PHENO",
    "PUBMED",
    "MOTIF_NAME",
    "MOTIF_POS",
    "HIGH_INF_POS",
    "MOTIF_SCORE_CHANGE",
    "BLOSUM62",
    "CSN",
    "DownstreamProtein",
    "ProteinLengthChange",
    "GO",
    "LoFtool",
    "TSSDistance",
    "MaxEntScan_alt",
    "MaxEntScan_diff",
    "MaxEntScan_ref",
    "MPC",
    "ExACpLI",
    "gnomADg",
    "gnomADg_AF_AFR",
    "gnomADg_AF_AMR",
    "gnomADg_AF_ASJ",
    "gnomADg_AF_EAS",
    "gnomADg_AF_FIN",
    "gnomADg_AF_NFE",
    "gnomADg_AF_OTH",
]

MAX_AF = 0.001


def germ_snv(file_name, logger, annotation, metadata):
    ppcg_ids_map = collect_ppcg_samples_ids(metadata, logger)
    aberration_data = []
    n = 0
    pn = 0
    t = 0
    # dtypes={"chrom":str, "start":int, "end":int}
    # return pd.read_csv("../bi_ppcg/results/aberration_0.tsv", sep="\t").astype(dtypes)
    with VariantFile(file_name, "rb") as germline_snv:
        annotation_info_fields = parse_description(
            germline_snv.header.info["CSQ"].description
        )
        consequences_idx = annotation_info_fields.index("Consequence")
        impact_idx = annotation_info_fields.index("IMPACT")
        sift_idx = annotation_info_fields.index("SIFT")
        polyphen_idx = annotation_info_fields.index("PolyPhen")
        af_idx = annotation_info_fields.index("AF")
        gnomad_af_idx = annotation_info_fields.index("gnomAD_AF")
        symbol_idx = annotation_info_fields.index("SYMBOL")
        clin_sign_idx = annotation_info_fields.index("CLIN_SIG")
        samples_list = list(germline_snv.header.samples)
        random_id_to_ppcg = map_samples_id(ppcg_ids_map, samples_list, logger)
        for rec in germline_snv:
            t += 1
            info_csq = rec.info["CSQ"][0].split("|")
            clin_var_pathogenic = False
            if "pathogenic" in info_csq[clin_sign_idx]:
                clin_var_pathogenic = True
            try:
                af = float(info_csq[af_idx])
            except ValueError:
                af = None
            try:
                g_af = float(info_csq[gnomad_af_idx])
            except ValueError:
                g_af = None
            if af is None and g_af is None and not clin_var_pathogenic:
                continue
            else:
                if af is None:
                    af = g_af
            process_genotype = False
            if af and clin_var_pathogenic and af <= MAX_AF:
                impact = info_csq[impact_idx]
                process_genotype = True
                # if clin_var_pathogenic or impact == "HIGH":
                #    process_genotype = True
                # elif impact == "MODERATE":
                #    sift = info_csq[sift_idx]
                #    polyphen = info_csq[polyphen_idx]
                #    if "deleterious" in sift or "damaging" in polyphen:
                #        process_genotype = True
            if process_genotype:
                cons = info_csq[consequences_idx]
                n += 1
                for sample, genotype in rec.samples.items():
                    genotype_sum = sum(genotype["GT"])
                    if genotype_sum >= 1:
                        pn += 1
                        if genotype_sum == 2:
                            aberration_type = AberrationType.GERM_HOM_SNV
                        else:
                            aberration_type = AberrationType.GERM_SNV
                        aberration_data.append(
                            Aberration(
                                **{
                                    "chrom": rec.chrom,
                                    "start": rec.start,
                                    "end": rec.stop,
                                    "aberration_type": aberration_type,
                                    "aberration_subtype": cons,
                                    "gene": info_csq[symbol_idx],
                                    "sample_id": random_id_to_ppcg[sample],
                                }
                            )
                        )
    logger.info(
        "Parsed %i records and found %i aberrations in %i variants"
        % (t, pn, n)
    )
    return pd.DataFrame(vars(a) for a in aberration_data)


def parse_description(content):
    value = content.strip('"').split(":")
    if len(value) == 2:
        value_strip = [
            i.strip().strip("'") for i in value[1].strip(" ").split("|")
        ]
        return value_strip
    else:
        return value[0]


def collect_ppcg_samples_ids(metadata, logger):
    logger.info("Open PPCG metadata %s" % metadata)
    ppcg_sample_ids = {
        "eo": {},
        "local": {},
        "sanger": {},
    }
    with xopen(metadata, "rt") as ppcg_info:
        header = next(ppcg_info).strip().split(",")
        eo_id_idx = header.index("EO_UUID")
        ppcg_id_idx = header.index("PPCG_Sample_ID")
        local_id_idx = header.index("Local_ID")
        sanger_id_idx = header.index("Sanger_UUID")
        for line in ppcg_info:
            line = line.strip().split(",")
            ppcg_sample_ids["eo"][line[eo_id_idx]] = line[ppcg_id_idx]
            ppcg_sample_ids["local"][line[local_id_idx]] = line[ppcg_id_idx]
            ppcg_sample_ids["sanger"][line[sanger_id_idx]] = line[ppcg_id_idx]
    logger.info(
        "Recorder %i sample information" % len(ppcg_sample_ids["eo"].keys())
    )
    return ppcg_sample_ids


def map_samples_id(sample_map, samples, logger):
    sample_dict = {}
    for sample in samples:
        if sample in sample_map["sanger"]:
            sample_dict[sample] = sample_map["sanger"][sample]
        elif sample in sample_map["local"]:
            sample_dict[sample] = sample_map["local"][sample]
        elif sample in sample_map["eo"]:
            sample_dict[sample] = sample_map["eo"][sample]
        else:
            logger.warning(
                "Skip sample %s as it wasn't mapped in the metadata" % sample
            )
    return sample_dict
