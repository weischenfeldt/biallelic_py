from biallelic.misc import xopen
from biallelic.models import SampleDonor, Gender
import pandas as pd


# cellularity     ploidy  SLPP    Sample


def sample_donors(file_path, logger):
    donors_info = []
    logger.info("Read Sample/Donor information from %s" % file_path)
    with xopen(file_path, "rt") as ppcg_meta:
        header = next(ppcg_meta).strip().split("\t")
        sample_id_idx = header.index("Sample")
        cellularity_idx = header.index("cellularity")
        ploidy_idx = header.index("ploidy")

        for line in ppcg_meta:
            line = line.strip().split("\t")
            donors_info.append(
                SampleDonor(
                    line[sample_id_idx],
                    line[sample_id_idx],
                    Gender.Male,
                    float(line[ploidy_idx]),
                    float(line[cellularity_idx])
                )
            )
    logger.info("Recorded %i Sample/Donor information" % len(donors_info))
    return pd.DataFrame(vars(d) for d in donors_info)
