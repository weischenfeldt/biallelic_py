from biallelic.misc import xopen
import pandas as pd


class Gene:
    def __init__(
        self, chrom: str, start: int, end: int, gene: str, strand: str
    ):
        self.chrom = str(chrom)
        self.start = int(start)
        self.end = int(end)
        self.gene = str(gene)
        self.strand = str(strand)


def exons(file_path, logger):
    exon_list = []
    logger.info("Read Exons from %s in BED  format" % file_path)
    with xopen(file_path, "rt") as exons_bed:
        header = next(exons_bed).strip().split("\t")
        chrom_idx = header.index("chrom")
        start_idx = header.index("start")
        end_idx = header.index("end")
        gene_idx = header.index("gene_name")
        strand_idx = header.index("strand")
        type_idx = header.index("type")
        for line in exons_bed:
            line = line.strip().split("\t")
            if line[type_idx] == "exon":
                exon_list.append(
                    Gene(
                        line[chrom_idx],
                        line[start_idx],
                        line[end_idx],
                        line[gene_idx],
                        line[strand_idx],
                    )
                )
    logger.info("Recorded %i exons" % len(exon_list))
    return pd.DataFrame(vars(e) for e in exon_list)


def genes(file_path, logger):
    genes = []
    logger.info("Read Genes from %s in BED  format" % file_path)
    with xopen(file_path, "rt") as genes_bed:
        for line in genes_bed:
            if not line.startswith("#"):
                line = line.strip().split("\t")
                genes.append(Gene(line[0], line[1], line[2], line[3], line[4]))
    logger.info("Recorded %i genes" % len(genes))
    return pd.DataFrame(vars(g) for g in genes)
