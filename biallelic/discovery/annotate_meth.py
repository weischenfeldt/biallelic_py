import os
from biallelic.models import DoubleHit, DoubleHitType, OmicsType
from biallelic.misc import make_ucsc_format
import pandas as pd
import bioframe as bf


def main(aberration_list, output_path, annotation, title, logger):
    double_hits = []
    output_file = os.path.join(
        output_path, "result_discovery_annotated_methylation.tsv"
    )
    aberrations = pd.concat(aberration_list, ignore_index=True)
    donor_map = annotation["sample_donors"].set_index("sample_id")[["donor_id", "matching_sample_id", "omics"]]
    aberrations = pd.merge(aberrations, donor_map, on="sample_id")

    aberrations_patients = aberrations.groupby("sample_id")
    patients = list(aberrations_patients.groups.keys())
    logger.info("Divide the aberrations over %i samples" % len(patients))
    n_found = 0
    for i in range(len(patients)):
        patient = aberrations_patients.get_group(patients[i])
        try:
            if donor_map.loc[patients[i]].omics != OmicsType.Methylomics:
                continue
            if donor_map.loc[patients[i]].matching_sample_id == "NA" or donor_map.loc[patients[i]].matching_sample_id == "":
                 continue
            matching_index = patients.index(donor_map.loc[patients[i]].matching_sample_id)
            patient_genomics = aberrations_patients.get_group(patients[matching_index])
        except ValueError:
            continue
        patient_meth = patient.query('type == "METHYL"')
        patient_het_loss = patient_genomics.query('type == "HET_LOSS" & n_copy == 1 & vaf >= 0.9')
        meth_overlap_genes = bf.overlap(
            patient_meth,
            annotation["genes"],
            how="inner",
            suffixes=("", "_gene"),
        )
        if meth_overlap_genes.shape[0] > 0:
            annotated_meth = bf.overlap(
                patient_het_loss,
                meth_overlap_genes,
                how="inner",
                suffixes=("", "_meth"),
            )
            if annotated_meth.shape[0] > 0:
                annotated_meth.drop_duplicates(
                    subset=[
                        "chrom",
                        "start",
                        "end",
                        "chrom_meth",
                        "start_meth",
                        "end_meth",
                    ],
                    keep="first",
                    inplace=True,
                )
                for index, meth in annotated_meth.iterrows():
                    double_hits.append(
                        make_double_hits(meth, DoubleHitType.SomLoss_Methyl)
                    )
        n_found += 1
    logger.info(
        "Found %i biallelic inactivation over %i samples"
        % (len(double_hits), n_found)
    )
    pd.DataFrame(vars(a) for a in double_hits).to_csv(
        output_file, sep="\t", index=False
    )


def make_double_hits(meth: pd.DataFrame, hit_type: DoubleHitType):
    hits = DoubleHit(
        **{
            "gene": meth.gene_gene_meth,
            "cytoband": ".",
            "first_hit": make_ucsc_format(meth.chrom, meth.start, meth.end),
            "first_hit_type": meth.type,
            "second_hit": make_ucsc_format(
                meth.chrom_meth, meth.start_meth, meth.end_meth
            ),
            "second_hit_type": meth.subtype_meth,
            "hit_type": hit_type,
            "sample_id": meth.sample_id,
            "donor_id": meth.donor_id,
            "id": meth.id_meth
        }
    )
    return hits
