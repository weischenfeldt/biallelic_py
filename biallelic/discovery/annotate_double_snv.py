import os
from biallelic.models import DoubleHit, DoubleHitType
from biallelic.misc import make_ucsc_format
import pandas as pd
import bioframe as bf

min_vaf = 0.4
min_dist = 10000


def main(aberration_list, output_path, annotation, title, logger):
    double_hits = []
    output_file = os.path.join(
        output_path, "result_discovery_annotated_double_snv.tsv"
    )
    aberrations = pd.concat(aberration_list, ignore_index=True)
    donor_map = annotation["sample_donors"].set_index("sample_id")["donor_id"]
    aberrations = pd.merge(aberrations, donor_map, on="sample_id")
    aberrations_patients = aberrations.groupby("sample_id")
    patients = list(aberrations_patients.groups.keys())
    logger.info("Divide the aberrations over %i samples" % len(patients))
    for i in range(len(patients)):
        patient = aberrations_patients.get_group(patients[i])
        patient_snv = patient.query('type == "SNV" | type == "INDEL"')
        snv_overlap_genes = bf.overlap(
            patient_snv,
            annotation["genes"],
            how="inner",
            suffixes=("", "_gene"),
        )
        snv_overlap_genes.drop_duplicates(
            subset=["chrom", "start", "end"],
            keep="first",
            inplace=True,
        )
        genes_groups = snv_overlap_genes.groupby("gene_gene").groups
        double_mutations = {
            k: v for k, v in genes_groups.items() if len(v) == 2
        }
        for gene, indexes in double_mutations.items():
            hits = snv_overlap_genes.loc[indexes]
            snv_1 = hits.loc[indexes[0]]
            snv_2 = hits.loc[indexes[1]]
            #if snv_1.vaf >= min_vaf and snv_2.vaf >= min_vaf:
            if abs(snv_1.start - snv_2.start) >= min_dist:
                    double_hits.append(
                        DoubleHit(
                            **{
                                "gene": gene,
                                "cytoband": ".",
                                "first_hit": make_ucsc_format(
                                    snv_1.chrom,
                                    snv_1.start,
                                    snv_1.end,
                                ),
                                "first_hit_type": snv_1.type,
                                "second_hit": make_ucsc_format(
                                    snv_2.chrom,
                                    snv_2.start,
                                    snv_2.end,
                                ),
                                "second_hit_type": "%s/%s"
                                % (snv_1.subtype, snv_2.subtype),
                                "hit_type": DoubleHitType.SomSnv_SomSnv,
                                "sample_id": snv_2.sample_id,
                                "donor_id": snv_2.donor_id,
                            }
                        )
                    )
        i += 1
    logger.info(
        "Found %i biallelic inactivation over %i samples"
        % (len(double_hits), i)
    )
    pd.DataFrame(vars(a) for a in double_hits).to_csv(
        output_file, sep="\t", index=False
    )
