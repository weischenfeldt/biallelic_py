import os
from biallelic.models import DoubleHit, DoubleHitType
from biallelic.misc import make_ucsc_format
import pandas as pd
import bioframe as bf


def main(aberration_list, output_path, annotation, title, logger):
    double_hits = []
    output_file = os.path.join(
        output_path, "result_discovery_annotated_germ_snv.tsv"
    )
    aberrations = pd.concat(aberration_list, ignore_index=True)
    # FIXME hotfix since the new metadata don't have normal samples anymore (need fix)
    logger.warning(
        "Assuming donor_id correspond to the first 8 char of sample id. "
        "The new metadata don't have normal samples anymore (need fix)"
    )

    aberrations["donor_id"] = aberrations["sample_id"].str[:8]
    #donor_map = annotation["sample_donors"].set_index("sample_id")["donor_id"]
    #aberrations = pd.merge(aberrations, donor_map, on="sample_id")
    aberrations_patients = aberrations.groupby("donor_id")
    patients = list(aberrations_patients.groups.keys())
    logger.info("Divide the aberrations over %i patients" % len(patients))
    for i in range(len(patients)):
        patient = aberrations_patients.get_group(patients[i])
        patient_germ_snv = patient.query(
            'type == "GERM_SNV" | type == "GERM_HOM_SNV"'
        )
        patient_het_loss = patient.query('type == "HET_LOSS" & n_copy == 1')
        if patient_germ_snv.shape[0] > 0:
            double_hits += handle_germ_snv(
                patient_het_loss, patient_germ_snv, annotation, logger
            )
    logger.info(
        "Found %i biallelic inactivation over %i patients"
        % (len(double_hits), len(patients))
    )
    pd.DataFrame(vars(a) for a in double_hits).to_csv(
        output_file, sep="\t", index=False
    )


def handle_germ_snv(scna_df, snp_df, annotation, logger):
    double_hits = []
    snp_overlap_genes = bf.overlap(
        snp_df,
        annotation["genes"],
        how="inner",
        suffixes=("", "_gene"),
    )
    scna_overlap_genes = bf.overlap(
        scna_df,
        annotation["genes"],
        how="inner",
        suffixes=("", "_gene"),
    )

    if scna_overlap_genes.shape[0] > 0:
        if snp_overlap_genes.shape[0] > 0:
            hits_genes = list(
                set(scna_overlap_genes.gene_gene).intersection(
                    snp_overlap_genes.gene_gene
                )
            )
            if len(hits_genes) > 0:
                for gene in hits_genes:
                    gene_hits_snp = snp_overlap_genes[
                        snp_overlap_genes.gene_gene == gene
                    ]
                    gene_hits_scna = scna_overlap_genes[
                        scna_overlap_genes.gene_gene == gene
                    ].min()
                    for index, snp in gene_hits_snp.iterrows():
                        double_hits.append(
                            DoubleHit(
                                **{
                                    "gene": snp.gene_gene,
                                    "cytoband": ".",
                                    "first_hit": make_ucsc_format(
                                        snp.chrom, snp.start, snp.end
                                    ),
                                    "first_hit_type": snp.type,
                                    "second_hit": make_ucsc_format(
                                        gene_hits_scna.chrom,
                                        gene_hits_scna.start,
                                        gene_hits_scna.end,
                                    ),
                                    "second_hit_type": gene_hits_scna.subtype,
                                    "hit_type": DoubleHitType.GermSnp_SomLoss,
                                    "sample_id": snp.sample_id,
                                    "donor_id": snp.donor_id,
                                }
                            )
                        )
    return double_hits
