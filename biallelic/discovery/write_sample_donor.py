import os


def main(aberration_list, output_path, annotation, title, logger):
    i = 0
    sample_donor = annotation["sample_donors"]
    output_file = os.path.join(output_path, "samples_donor_info.tsv")
    logger.info("Write Sample info file to %s" % output_file)
    sample_donor.to_csv(output_file, sep="\t", index=False)
