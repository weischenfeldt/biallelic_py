import os
from biallelic.models import DoubleHit, DoubleHitType
from biallelic.misc import make_ucsc_format
import pandas as pd
import bioframe as bf

min_vaf = 0.9

def main(aberration_list, output_path, annotation, title, logger):
    double_hits = []
    output_file = os.path.join(
        output_path, "result_discovery_annotated_snv.tsv"
    )
    aberrations = pd.concat(aberration_list, ignore_index=True)
    donor_map = annotation["sample_donors"].set_index("sample_id")["donor_id"]
    aberrations = pd.merge(aberrations, donor_map, on="sample_id")
    aberrations_patients = aberrations.groupby("sample_id")
    patients = list(aberrations_patients.groups.keys())
    logger.info("Divide the aberrations over %i samples" % len(patients))
    n_found = 0
    for i in range(len(patients)):
        patient = aberrations_patients.get_group(patients[i])
        patient_snv = patient.query('type == "SNV"')
        patient_het_loss = patient.query('type == "HET_LOSS" & n_copy == 1 & vaf < 0.9')
        snv_overlap_genes = bf.overlap(
            patient_snv,
            annotation["genes"],
            how="inner",
            suffixes=("", "_gene"),
        )
        if snv_overlap_genes.shape[0] > 0:
            annotated_snv = bf.overlap(
                patient_het_loss,
                snv_overlap_genes,
                how="inner",
                suffixes=("", "_snv"),
            )
            if annotated_snv.shape[0] > 0:
                annotated_snv.drop_duplicates(
                    subset=[
                        "chrom",
                        "start",
                        "end",
                        "chrom_snv",
                        "start_snv",
                        "end_snv",
                    ],
                    keep="first",
                    inplace=True,
                )
                for index, snv in annotated_snv.iterrows():
                    if snv.vaf_snv >= min_vaf:
                        double_hits.append(
                            make_double_hits(snv, DoubleHitType.SubclonalLoss_SomSnv)
                        )
                    else:
                        pass
        n_found += 1
    logger.info(
        "Found %i biallelic inactivation over %i samples"
        % (len(double_hits), n_found)
    )
    pd.DataFrame(vars(a) for a in double_hits).to_csv(
        output_file, sep="\t", index=False
    )


def make_double_hits(snv: pd.DataFrame, hit_type: DoubleHitType):
    hits = DoubleHit(
        **{
            "gene": snv.gene_gene_snv,
            "cytoband": ".",
            "first_hit": make_ucsc_format(snv.chrom, snv.start, snv.end),
            "first_hit_type": snv.type,
            "second_hit": make_ucsc_format(
                snv.chrom_snv, snv.start_snv, snv.end_snv
            ),
            "second_hit_type": snv.subtype_snv,
            "hit_type": hit_type,
            "sample_id": snv.sample_id,
            "donor_id": snv.donor_id,
        }
    )
    return hits
