import os
import re
import yaml
import pandas as pd
from biallelic.models import DoubleHitType
from matplotlib.backends.backend_pdf import PdfPages
import matplotlib.pyplot as plt
import pyoncoprint


def main(aberration_list, output_path, annotation, title, logger):
    results_list = [
        r
        for r in os.listdir(output_path)
        if (r.startswith("result_discovery") and r.endswith(".tsv"))
    ]
    if len(results_list) == 0:
        logger.error("No results found for the summary")
        return
    logger.info("Found %i output results for the summary" % len(results_list))
    biallelic_inactivation_data = []
    output_summary = os.path.join(output_path, "summary_plots.pdf")
    output_file = os.path.join(
        output_path, "biallelic_inactivation_merged_table.tsv"
    )
    output_filtered_file = os.path.join(
        output_path, "biallelic_inactivation_filtered_merged_table.tsv"
    )
    bystanders_map_file = os.path.join(
        output_path, "biallelic_inactivation_bystanders_map.yaml"
    )
    for result_file in results_list:
        try:
            biallelic_inactivation_data += [
                pd.read_csv(os.path.join(output_path, result_file), sep="\t")
            ]
        except pd.errors.EmptyDataError:
            logger.warning("Error, empty file found in %s" % result_file)
    biallelic_inactivations = pd.concat(
        biallelic_inactivation_data, ignore_index=True
    )

    # chrom_range = pd.DataFrame(
    #     biallelic_inactivations.first_hit.str.split(":").tolist(),
    #     columns=["chrom", "range"],
    # ).astype({"chrom": str, "range": str})
    # start_end = pd.DataFrame(
    #     chrom_range.range.str.split("-").tolist(),
    #     columns=["start", "end"],
    # ).astype({"start": int, "end": int})
    biallelic_inactivations.to_csv(output_file, sep="\t", index=False)



    # cleanup the gene list
    # remove gene names where the coordiantes point to different chromosomes

    biallelic_inactivations = cleanup_jumping_genes(biallelic_inactivations, logger=logger)

    # Remove calls where scna and SV-edges don't overlap
    biallelic_inactivations = cleanup_sv(biallelic_inactivations, logger)

    # keep only one gene per event in the hom_loss calls

    biallelic_inactivations = cleanup_hom_loss(biallelic_inactivations, logger)
    biallelic_inactivations = cleanup_bystanders(biallelic_inactivations, annotation, logger)
    biallelic_inactivations.to_csv(output_filtered_file, sep="\t", index=False)

    biallelic_inactivations.drop_duplicates(
        subset=["gene", "donor_id"], keep="first", inplace=True
    )

    with PdfPages(output_summary) as pdf:
        biallelic_inactivations.gene.value_counts()[:20].plot(kind="bar")
        plt.title("Biallelic inactivations counts per gene")
        plt.xlabel("Gene symbol")
        plt.ylabel("Patients counts")
        plt.gcf().tight_layout()
        pdf.savefig()
        plt.close()

        donor_gene = biallelic_inactivations.pivot_table(
            values="hit_type", index="gene", columns="donor_id", aggfunc="max"
        )
        donor_gene_top = donor_gene[
            donor_gene.isnull().sum(axis=1) < len(donor_gene.columns) - 2
        ].dropna(axis=1, how="all")

        found_hits = set(sum(donor_gene_top.values.tolist(), []))
        possible_hit_types = set(dht.__str__() for dht in DoubleHitType)
        hit_types = list(possible_hit_types.intersection(found_hits))

        cmap = plt.get_cmap("Set2")
        mutation_markers = {
            hit_types[i]: dict(marker="fill", color=cmap(i), zindex=0)
            for i in range(len(hit_types))
        }
        op = pyoncoprint.OncoPrint(donor_gene_top)
        try:
            fig, axes = op.oncoprint(
                mutation_markers,
                title=title,
                is_legend=True,
                is_rightplot=True,
                gap=0.1,
                figsize=[21, 50],
            )
        except TypeError:
            fig, axes = op.oncoprint(mutation_markers, title=title, gap=0.1)
        fig.tight_layout()
        pdf.savefig(fig, facecolor="white")
        plt.close()


def cleanup_jumping_genes(df, logger):
    hits_genes = df.groupby("gene")
    genes = list(hits_genes.groups.keys())
    blacklist = []
    for i in range(len(genes)):
        gene = hits_genes.get_group(genes[i])
        chromosome_position = gene.first_hit.str.split(":", expand=True)
        unque_chr = chromosome_position.iloc[:, 0].unique().tolist()
        if len(unque_chr) > 1:
            blacklist.append(genes[i])
            logger.info("Cleanup, removing meta genes %s from the calls", genes[i])
    mask = df['gene'].isin(blacklist)
    df = df.loc[~mask]
    return(df)


def cleanup_hom_loss(df, logger):
    mask = df.first_hit_type.isin(["HOM_LOSS"])
    hom_loss = df.loc[mask]
    df = df.loc[~mask]
    hits_counts_per_gene = {}
    for value in df.gene:
        hits_counts_per_gene[value] = hits_counts_per_gene.get(value, 0) + 1
    logger.info("Cleaning up %i HOM_LOSS calls" % len(hom_loss))
    hom_loss_first_hit = hom_loss.groupby("first_hit")
    first_hits = list(hom_loss_first_hit.groups.keys())
    candidate_hom_losses = []
    for i in range(len(first_hits)):
        first_hit = hom_loss_first_hit.get_group(first_hits[i])
        unque_genes = first_hit.gene.unique().tolist()
        if len(unque_genes) > 1:
            top_score = 0
            candidate_genes = []
            for gene in unque_genes:
                gene_score = hits_counts_per_gene.get(gene, 0)
                if gene_score > top_score:
                    top_score = gene_score
                    candidate_genes = [gene]
                elif gene_score == top_score:
                    candidate_genes.append(gene)
            logger.info("Keep gene(s) %s as candidate(s) for the HOM_LOSS in %s" % (
                ", ".join(candidate_genes), first_hits[i]))
            mask_candidate = first_hit.gene.isin(candidate_genes)
            candidate_hom_losses.append(first_hit[mask_candidate])
        else:
            candidate_hom_losses.append(first_hit)
    cleaned_hom_loss = pd.concat(
        candidate_hom_losses, ignore_index=True)
    logger.info("Returning %i HOM_LOSS cleaned calls" % len(cleaned_hom_loss))
    return pd.concat([cleaned_hom_loss, df])


def cleanup_sv(df, logger):
    mask = df.hit_type.isin(["som_loss/som_sv"])
    sv_hits = df.loc[mask]
    df = df.loc[~mask]
    logger.info("Cleaning up %i som_loss/som_sv calls" % len(sv_hits))
    keep_rows = []
    for index, row in sv_hits.iterrows():
        is_sv_inlcuded_in_scna = False
        first_hit_coord = re.split(':|-', row.first_hit)
        second_hit_coord = re.split(':|-', row.second_hit)
        if row.second_hit_type == "TRA":
            is_sv_inlcuded_in_scna = True
        elif int(first_hit_coord[1]) < int(second_hit_coord[1]) and int(first_hit_coord[2]) > int(second_hit_coord[1]):
            is_sv_inlcuded_in_scna = True
        elif int(first_hit_coord[1]) < int(second_hit_coord[2]) and int(first_hit_coord[2]) > int(second_hit_coord[2]):
            is_sv_inlcuded_in_scna = True
        if is_sv_inlcuded_in_scna:
            keep_rows.append(index)

    cleaned_sv_hits = sv_hits.loc[keep_rows]
    logger.info("Returning %i som_loss/som_sv cleaned calls" % len(cleaned_sv_hits))
    return pd.concat([cleaned_sv_hits, df])


def cleanup_bystanders(df, annotation, logger):
    bystanders = annotation["bystanders"]
    for gene in bystanders:
        target_gene = bystanders[gene]["target"]
        # counts the bystander occurrence
        n_rep = df[(df["gene"] == gene) & (df["hit_type"] == "som_loss/som_loss")].donor_id.nunique()
        # replce with the target gene
        if n_rep > 0:
            logger.info("Recording %i ocurrences of %s to the bystanders map" % (n_rep, gene))
            df[df["hit_type"] == "som_loss/som_loss"].gene.replace(gene, target_gene, inplace=True)
    return(df)
