import os
from biallelic.models import DoubleHit, DoubleHitType
from biallelic.misc import make_ucsc_format
import pandas as pd
import bioframe as bf


def main(aberration_list, output_path, annotation, title, logger):
    double_hits = []
    output_file = os.path.join(
        output_path, "result_discovery_annotated_sv.tsv"
    )
    aberrations = pd.concat(aberration_list, ignore_index=True)
    donor_map = annotation["sample_donors"].set_index("sample_id")["donor_id"]
    aberrations = pd.merge(aberrations, donor_map, on="sample_id")
    aberrations_patients = aberrations.groupby("sample_id")
    patients = list(aberrations_patients.groups.keys())
    logger.info("Divide the aberrations over %i samples" % len(patients))
    for i in range(len(patients)):
        patient = aberrations_patients.get_group(patients[i])
        patient_sv_tra = patient.query('type == "SV" & subtype == "TRA"')
        patient_sv_not_tra = patient.query(
            'type == "SV" & not subtype == "TRA"'
        )
        patient_het_loss = patient.query('type == "HET_LOSS" & n_copy == 1 & vaf >= 0.9')
        if patient_sv_not_tra.shape[0] > 0:
            double_hits += handle_sv(
                patient_het_loss, patient_sv_not_tra, annotation, logger
            )
        if patient_sv_tra.shape[0] > 0:
            double_hits += handle_tra(
                patient_het_loss, patient_sv_tra, annotation, logger
            )
        i += 1
    logger.info(
        "Found %i biallelic inactivation over %i samples"
        % (len(double_hits), i)
    )
    pd.DataFrame(vars(a) for a in double_hits).to_csv(
        output_file, sep="\t", index=False
    )


def handle_tra(scna_df, sv_df, annotation, logger):
    double_hits = []
    sv_overlap_genes = bf.overlap(
        sv_df,
        annotation["genes"],
        how="inner",
        suffixes=("", "_gene"),
    )
    if sv_overlap_genes.shape[0] > 0:
        annotated_sv = bf.overlap(
            scna_df,
            sv_overlap_genes,
            how="inner",
            suffixes=("", "_sv"),
        )
        annotated_sv.drop_duplicates(
            subset=[
                "chrom",
                "start",
                "end",
                "chrom_sv",
                "start_sv",
                "end_sv",
            ],
            keep="first",
            inplace=True,
        )
        if annotated_sv.shape[0] > 0:
            for index, sv in annotated_sv.iterrows():
                double_hits.append(
                    DoubleHit(
                        **{
                            "gene": sv.gene_gene_sv,
                            "cytoband": ".",
                            "first_hit": make_ucsc_format(
                                sv.chrom, sv.start, sv.end
                            ),
                            "first_hit_type": sv.type,
                            "second_hit": make_ucsc_format(
                                sv.chrom_sv, sv.start_sv, sv.end_sv
                            ),
                            "second_hit_type": sv.subtype_sv,
                            "hit_type": DoubleHitType.SomLoss_SomSv,
                            "sample_id": sv.sample_id,
                            "donor_id": sv.donor_id,
                        }
                    )
                )
    return double_hits


def handle_sv(scna_df, sv_df, annotation, logger):
    double_hits = []
    sv_overlap_genes = bf.overlap(
        sv_df,
        annotation["exons"],
        how="inner",
        suffixes=("", "_exon"),
    )
    if sv_overlap_genes.shape[0] > 0:
        annotated_sv = bf.overlap(
            scna_df,
            sv_overlap_genes,
            how="inner",
            suffixes=("", "_sv"),
        )
        annotated_sv.drop_duplicates(
            subset=[
                "chrom",
                "start",
                "end",
                "chrom_sv",
                "start_sv",
                "end_sv",
            ],
            keep="first",
            inplace=True,
        )
        if annotated_sv.shape[0] > 0:
            for index, sv in annotated_sv.iterrows():
                if sv.start <= sv.start_exon_sv and sv.end >= sv.end_exon_sv:
                    continue
                double_hits.append(
                    DoubleHit(
                        **{
                            "gene": sv.gene_exon_sv,
                            "cytoband": ".",
                            "first_hit": make_ucsc_format(
                                sv.chrom, sv.start, sv.end
                            ),
                            "first_hit_type": sv.type,
                            "second_hit": make_ucsc_format(
                                sv.chrom_sv, sv.start_sv, sv.end_sv
                            ),
                            "second_hit_type": sv.subtype_sv,
                            "hit_type": DoubleHitType.SomLoss_SomSv,
                            "sample_id": sv.sample_id,
                            "donor_id": sv.donor_id,
                        }
                    )
                )
    return double_hits
