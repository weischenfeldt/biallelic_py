import os
import pandas as pd
from biallelic.models import DoubleHitType
from matplotlib.backends.backend_pdf import PdfPages
import matplotlib.pyplot as plt
import pyoncoprint


def main(aberration_list, output_path, annotation, title, logger):
    results_list = [
        r
        for r in os.listdir(output_path)
        if (r.startswith("result_discovery") and r.endswith(".tsv"))
    ]
    if len(results_list) == 0:
        logger.error("No results found for the summary")
        return
    logger.info("Found %i output results for the summary" % len(results_list))
    biallelic_inactivation_data = []
    output_summary = os.path.join(output_path, "summary_oncoprint.png")
    output_file = os.path.join(
        output_path, "biallelic_inactivation_merged_table.tsv"
    )
    for result_file in results_list:
        try:
            biallelic_inactivation_data += [
                pd.read_csv(os.path.join(output_path, result_file), sep="\t")
            ]
        except pd.errors.EmptyDataError:
            logger.warning("Error, empty file found in %s" % result_file)
    biallelic_inactivations = pd.concat(
        biallelic_inactivation_data, ignore_index=True
    )

    biallelic_inactivations.drop_duplicates(
        subset=["gene", "donor_id"], keep="first", inplace=True
    )

    donor_gene = biallelic_inactivations.pivot_table(
        values="hit_type", index="gene", columns="donor_id", aggfunc="max"
    )
    donor_gene_top = donor_gene[
        donor_gene.isnull().sum(axis=1) < len(donor_gene.columns) - 4
    ].dropna(axis=1, how="all")

    found_hits = set(sum(donor_gene_top.values.tolist(), []))
    possible_hit_types = set(dht.__str__() for dht in DoubleHitType)
    hit_types = list(possible_hit_types.intersection(found_hits))

    cmap = plt.get_cmap("Set2")
    mutation_markers = {
        hit_types[i]: dict(marker="fill", color=cmap(i), zindex=0)
        for i in range(len(hit_types))
    }
    op = pyoncoprint.OncoPrint(donor_gene_top)
    try:
        fig, axes = op.oncoprint(
            mutation_markers,
            title=title,
            is_legend=True,
            is_rightplot=True,
            gap=0.1,
            figsize=[21, 8],
        )
    except TypeError:
        fig, axes = op.oncoprint(mutation_markers, title=title, gap=0.1)
    fig.tight_layout()
    fig.savefig(output_summary, facecolor="white")
    plt.close()
