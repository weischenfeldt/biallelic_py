import os


def main(aberration_list, output_path, annotation, title, logger):
    i = 0
    for aberration in aberration_list:
        output_file = os.path.join(output_path, "aberration_%i.tsv" % i)
        logger.info("Write aberration file to %s" % output_file)
        if aberration is not None:
            aberration.to_csv(output_file, sep="\t", index=False)
        i += 1
