import os
from biallelic.models import DoubleHit, DoubleHitType
from biallelic.misc import make_ucsc_format
import pandas as pd
import bioframe as bf


def main(aberration_list, output_path, annotation, title, logger):
    double_hits = []
    output_file = os.path.join(
        output_path, "result_discovery_homozygous_inactivation.tsv"
    )
    aberrations = pd.concat(aberration_list, ignore_index=True)
    donor_map = annotation["sample_donors"].set_index("sample_id")["donor_id"]
    aberrations = pd.merge(aberrations, donor_map, on="sample_id")
    aberrations_patients = aberrations.groupby("donor_id")
    patients = list(aberrations_patients.groups.keys())
    logger.info("Divide the aberrations over %i patients" % len(patients))
    for i in range(len(patients)):
        patient = aberrations_patients.get_group(patients[i])
        patient_hom_loss = patient.query('type == "HOM_LOSS" & vaf >= 0.9')
        patient_germ_hom_loss = patient.query('type == "GERM_HOM_LOSS"')
        patient_germ_hom_snv = patient.query('type == "GERM_HOM_SNV"')
        if patient_hom_loss.shape[0] > 0:
            double_hits += record_double_hits(
                patient_hom_loss,
                annotation,
                DoubleHitType.SomLoss_SomLoss,
                logger,
            )
        if patient_germ_hom_loss.shape[0] > 0:
            double_hits += record_double_hits(
                patient_germ_hom_loss,
                annotation,
                DoubleHitType.GermLoss_GermLoss,
                logger,
            )
        # if patient_germ_hom_snv.shape[0] > 0:
        #    double_hits += record_double_hits(
        #        patient_germ_hom_snv,
        #        annotation,
        #        DoubleHitType.GermSnp_GermSnp,
        #        logger,
        #    )
        i += 1
    logger.info(
        "Found %i biallelic inactivation over %i patients"
        % (len(double_hits), i)
    )
    pd.DataFrame(vars(a) for a in double_hits).to_csv(
        output_file, sep="\t", index=False
    )


def record_double_hits(
    df: pd.DataFrame, annotation: dict, hit_type: DoubleHitType, logger
):
    double_hits = []
    overlap_genes = bf.overlap(
        df,
        annotation["genes"],
        how="inner",
        suffixes=("", "_gene"),
    )

    overlap_genes.drop_duplicates(
        subset=["chrom", "start", "end", "gene_gene"], keep="first", inplace=True
    )
    if overlap_genes.shape[0] > 0:
        for index, rec in overlap_genes.iterrows():
            double_hits.append(
                DoubleHit(
                    **{
                        "gene": rec.gene_gene,
                        "cytoband": ".",
                        "first_hit": make_ucsc_format(
                            rec.chrom, rec.start, rec.end
                        ),
                        "first_hit_type": rec.type,
                        "second_hit": make_ucsc_format(
                            rec.chrom, rec.start, rec.end
                        ),
                        "second_hit_type": rec.subtype,
                        "hit_type": hit_type,
                        "sample_id": rec.sample_id,
                        "donor_id": rec.donor_id,
                    }
                )
            )
    return double_hits
