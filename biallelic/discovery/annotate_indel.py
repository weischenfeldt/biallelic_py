import os
from biallelic.models import DoubleHit, DoubleHitType
from biallelic.misc import make_ucsc_format
import pandas as pd
import bioframe as bf


def main(aberration_list, output_path, annotation, title, logger):
    double_hits = []
    output_file = os.path.join(
        output_path, "result_discovery_annotated_indel.tsv"
    )
    aberrations = pd.concat(aberration_list, ignore_index=True)
    donor_map = annotation["sample_donors"].set_index("sample_id")["donor_id"]
    aberrations = pd.merge(aberrations, donor_map, on="sample_id")
    aberrations_patients = aberrations.groupby("sample_id")
    patients = list(aberrations_patients.groups.keys())
    logger.info("Divide the aberrations over %i samples" % len(patients))
    for i in range(len(patients)):
        patient = aberrations_patients.get_group(patients[i])
        patient_snv = patient.query('type == "INDEL"')
        patient_het_loss = patient.query('type == "HET_LOSS" & n_copy == 1 & vaf >= 0.9')
        snv_overlap_genes = bf.overlap(
            patient_snv,
            annotation["genes"],
            how="inner",
            suffixes=("", "_gene"),
        )
        if snv_overlap_genes.shape[0] > 0:
            annotated_snv = bf.overlap(
                patient_het_loss,
                snv_overlap_genes,
                how="inner",
                suffixes=("", "_snv"),
            )
            if annotated_snv.shape[0] > 0:
                annotated_snv.drop_duplicates(
                    subset=[
                        "chrom",
                        "start",
                        "end",
                        "chrom_snv",
                        "start_snv",
                        "end_snv",
                    ],
                    keep="first",
                    inplace=True,
                )
                for index, snv in annotated_snv.iterrows():
                    double_hits.append(
                        DoubleHit(
                            **{
                                "gene": snv.gene_gene_snv,
                                "cytoband": ".",
                                "first_hit": make_ucsc_format(
                                    snv.chrom, snv.start, snv.end
                                ),
                                "first_hit_type": snv.type,
                                "second_hit": make_ucsc_format(
                                    snv.chrom_snv, snv.start_snv, snv.end_snv
                                ),
                                "second_hit_type": snv.subtype_snv,
                                "hit_type": DoubleHitType.SomLoss_SomIndel,
                                "sample_id": snv.sample_id,
                                "donor_id": snv.donor_id,
                            }
                        )
                    )
        i += 1
    logger.info(
        "Found %i biallelic inactivation over %i samples"
        % (len(double_hits), i)
    )
    pd.DataFrame(vars(a) for a in double_hits).to_csv(
        output_file, sep="\t", index=False
    )
