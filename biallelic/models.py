from enum import Enum
from biallelic.misc import camel_case_split


class Gender(Enum):
    Unknown = 0
    Male = 1
    Female = 2


class OmicsType(Enum):
    Genomics = 1
    Methylomics = 2
    Transcriptomics = 3


class AberrationType(Enum):
    SV = 1
    SNV = 2
    INDEL = 3
    CNN_LOH = 4
    GAIN_LOH = 5
    GERM_SNV = 6
    HOM_LOSS = 7
    HET_LOSS = 8
    GERM_HET_LOSS = 9
    GERM_HOM_LOSS = 10
    GERM_HOM_SNV = 11
    METHYL = 12
    AMP = 13
    GAIN = 14

class DoubleHitType(Enum):
    SomLoss_SomLoss = 1
    SomLoss_SomSnv = 2
    SomCnLoh_SomSnv = 3
    SomGainLoh_SomSnv = 4
    SomLoss_SomSv = 5
    SomSnv_SomSnv = 6
    SomLoss_SomIndel = 7
    GermLoss_SomLoss = 8
    GermLoss_GermLoss = 9
    GermLoss_GermSnp = 10
    GermLoss_GermSv = 11
    GermSnp_SomLoss = 12
    GermSv_SomLoss = 13
    GermSnp_GermSnp = 14
    SomLoss_Methyl = 15
    SubclonalLoss_SomSnv = 16
    SomLoss_SubclonalSnv = 17

    def __str__(self):
        hits = self.name.split("_")
        return "%s/%s" % (
            "_".join(camel_case_split(hits[0])).lower(),
            "_".join(camel_case_split(hits[1])).lower(),
        )


class SampleDonor:
    def __init__(
        self,
        sample_id: str,
        donor_id: str,
        gender: Gender,
        omics: OmicsType = OmicsType.Genomics,
        cellularity: float = 0,
        ploidy: float = 2,
        matching_sample_id: str = "",
    ) -> None:
        self.sample_id = sample_id
        self.donor_id = donor_id
        self.gender = gender.name
        self.omics = omics
        self.cellularity = cellularity
        self.ploidy = ploidy
        self.matching_sample_id = matching_sample_id


class Aberration:
    def __init__(
        self,
        chrom: str,
        start: int,
        end: int,
        aberration_type: AberrationType,
        aberration_subtype: str,
        sample_id: str,
        vaf: float = None,
        n_copy: int = None,
        gene: str = ".",
        id: str = ".",
    ):
        self.chrom = str(chrom)
        self.start = int(start)
        self.end = int(end)
        self.type = str(aberration_type.name)
        self.subtype = str(aberration_subtype)
        self.sample_id = str(sample_id)
        self.vaf = vaf
        self.n_copy = n_copy
        if self.vaf is not None:
            self.vaf = float(self.vaf)
        if self.n_copy is not None:
            self.n_copy = int(self.n_copy)
        self.gene = str(gene)
        self.id = str(id)


class DoubleHit:
    def __init__(
        self,
        gene: str,
        cytoband: str,
        first_hit: str,
        first_hit_type: str,
        second_hit: str,
        second_hit_type: str,
        hit_type: DoubleHitType,
        sample_id: str,
        donor_id: str,
        id: str = ".",
    ):
        self.gene = str(gene)
        self.cytoband = str(cytoband)
        self.first_hit = str(first_hit)
        self.first_hit_type = str(first_hit_type)
        self.second_hit = str(second_hit)
        self.second_hit_type = str(second_hit_type)
        self.hit_type = str(hit_type)
        self.sample_id = str(sample_id)
        self.donor_id = str(donor_id)
        self.id = str(id)
