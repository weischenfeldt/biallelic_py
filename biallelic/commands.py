#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
from biallelic.misc import DefaultHelpParser, SubcommandHelpFormatter
from biallelic.__version__ import VERSION, DATE, AUTHOR
from biallelic.logging import SimpleLogger
from biallelic.bi import Aberrations


def main():
    """
    Execute the function with args
    """
    parser = DefaultHelpParser(
        prog="biallelic_inactivation",
        formatter_class=lambda prog: SubcommandHelpFormatter(
            prog, max_help_position=20, width=75
        ),
        description=(
            "Find biallelic inactivations patterns "
            "in a multi-analysis sequencing cohort"
        ),
        epilog="This is version %s - %s - %s" % (VERSION, AUTHOR, DATE),
    )
    parser.add_argument(
        dest="manifest",
        type=str,
        help=(
            "Manifest in YAML format, containing the files path and description, relative to the current working directory"
        ),
    )

    args = parser.parse_args()

    output_folder = os.path.abspath(os.path.dirname(args.manifest))
    output_folder_results = os.path.join(output_folder, "results")
    output_folder_logs = os.path.join(output_folder, "logs")

    try:
        os.makedirs(output_folder_results)
    except FileExistsError:
        pass
    except Exception as e:
        print("error, please investigate %s" % e)

    main_logger = SimpleLogger("bi", output_folder_logs)

    discovery_group = Aberrations(args.manifest, main_logger)
    discovery_group.biallelic_inactivations(output_folder_results)


if __name__ == "__main__":
    main()
