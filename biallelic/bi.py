import yaml
import os
from biallelic.misc import xopen, package_modules, get_module_method
from biallelic import drivers, discovery


class Aberrations:
    def __init__(self, manifest_file, logger):
        """_summary_

        :param manifest_file: Path of the manifest YAML file
        :type manifest_file: str
        :param logger: Logger object passed from the upstream function
        :type logger: biallelic.logging.SimpleLogger
        """
        self.logger = logger
        self.manifest_file = manifest_file
        self.data_path = os.path.dirname(self.manifest_file)
        self.logger.log.info("Parse MANIFEST from %s" % manifest_file)
        with xopen(self.manifest_file, "rb") as manifest:
            self.manifest_content = yaml.safe_load(manifest)
        available_drivers = package_modules(drivers)
        available_discovery = package_modules(discovery)
        self.drivers_map = {d.split(".")[-1]: d for d in available_drivers}
        self.discovery_map = {d.split(".")[-1]: d for d in available_discovery}
        self.aberration_list = []
        self.reference_map = {}
        self.load_refs()
        self.load_contents()
        self.title = self.manifest_content["title"]

    def load_refs(self):
        for ref_item in self.manifest_content["ref"]:
            ref_metadata = self.manifest_content["ref"][ref_item]
            if os.path.isabs(ref_metadata["path"]):
                ref_path = ref_metadata["path"]
            else:
                ref_path = os.path.abspath(
                    os.path.join(self.data_path, ref_metadata["path"])
                )
            if ref_metadata["format_driver"] in self.drivers_map:
                sub_logger = self.logger.add_log(
                    "ref_%s_%s" % (ref_item, ref_metadata["format_driver"])
                )
                driver_method = get_module_method(
                    drivers, ref_metadata["format_driver"], ref_item
                )
                if driver_method is None:
                    sub_logger.log.error(
                        "Driver %s doesn't implement %s"
                        % (ref_metadata["format_driver"], ref_item)
                    )
                else:
                    self.reference_map[ref_item] = driver_method(
                        ref_path, sub_logger.log
                    )
            else:
                self.logger.log.error(
                    "Driver %s not supported, please write one"
                    % ref_metadata["format_driver"]
                )
        if not "sample_donors" in self.reference_map.keys():
            no_sample_donor_info_msg = (
                'The "sample_donors" annotation is missing. '
                "This means that some of the discovery analyses will "
                "not be able to match samples and patients. Please add a "
                '"sample_donors" in the metadata, in the "ref" section'
            )
            self.logger.log.error(no_sample_donor_info_msg)
            raise (Exception(no_sample_donor_info_msg))

    def load_contents(self):
        for input_item in self.manifest_content["input"]:
            driver = input_item["format_driver"]
            if driver in self.drivers_map:
                sub_logger = self.logger.add_log(
                    "%s_%s" % (input_item["type"], driver)
                )
                try:
                    extra_args = input_item["extra_driver_args"]
                except KeyError:
                    extra_args = {}
                if os.path.isabs(input_item["path"]):
                    input_path = input_item["path"]
                else:
                    input_path = os.path.join(
                        self.data_path, input_item["path"]
                    )
                self.load_aberration(
                    driver,
                    input_item["type"],
                    input_path,
                    extra_args,
                    sub_logger,
                )
            else:
                self.logger.log.error(
                    "Driver %s not supported, please write one" % driver
                )

    def load_aberration(
        self, driver, input_type, input_path, extra_args, logger
    ):
        driver_method = get_module_method(drivers, driver, input_type)
        if driver_method is None:
            logger.log.error(
                "Driver %s doesn't implement %s" % (driver, input_type)
            )
        else:
            self.aberration_list.append(
                driver_method(
                    input_path, logger.log, self.reference_map, **extra_args
                )
            )

    def biallelic_inactivations(self, output_path):
        for analysis in self.manifest_content["analyses"]:
            if analysis["name"] in self.discovery_map:
                analysis_method = get_module_method(
                    discovery, analysis["name"], "main"
                )
                sub_logger = self.logger.add_log(
                    "discovery_%s" % analysis["name"]
                )
                if analysis_method is None:
                    sub_logger.log.error(
                        "Discovery analysis %s doesn't implement a method named 'main'"
                        % analysis["name"]
                    )
                else:
                    analysis_method(
                        self.aberration_list,
                        output_path,
                        self.reference_map,
                        self.title,
                        sub_logger.log,
                    )
            else:
                self.logger.log.error(
                    "Analysis %s not supported, please write it"
                    % analysis["name"]
                )
