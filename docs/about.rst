.. index:: About


About
=====

Library to analize various genomic data over large cohorts
to discover possible biallelic inactivation patterns

.. image:: /_static/schematics_bi.png