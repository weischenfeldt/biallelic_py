.. index:: Test rRun

Test Run
===========

We try the method with some test data

Available Data
______________

We use the data from BRCA cohort in TCGA.
In order to speedup the analysis we limited the file to include only chromosome 17



Running the tool
________________

Manifest
--------

The `manifest.yaml` file define the input files and the order of the discovery
analysis.
To read more about the rules to follow in writing the `manifest.yaml` file see
to the :ref:`manifest` section.

Here an example manifest file:

.. literalinclude:: ../test/data/test_cbio/manifest.yaml
   :language: yaml


Execute the biallelic_inactivation command:
------------------------------------------


.. command-output:: biallelic_inactivation ../test/data/test_cbio/manifest.yaml
   :returncode: 0
   :shell:


After the command execute without errors, the results will be available in the
`results` folder at the same path of the `manifest.yaml` file

.. command-output:: ls ../test/data/test_cbio/results
   :returncode: 0
   :shell:


The content of the `results` folder depends on the analyses included in the manifest
file.

In our test example we included an oncoprint overview of the cohort 

.. admonition:: Fix the png

   To show the image we have to copy to the document root folder. This is a workaround
   of the documentation, totally unrelated to the library itself

   .. command-output:: cp ../test/data/test_cbio/results/summary_oncoprint.png ./_static/summary_oncoprint.png
      :shell:

   

.. image:: /_static/summary_oncoprint.png
