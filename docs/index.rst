Biallelic
=========

A python libary to standardize biallelic inactivation analysis

.. image:: /_static/schematics_bi.png

   
.. toctree::
   :maxdepth: 2

   about
   install
   example


Indexes and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
