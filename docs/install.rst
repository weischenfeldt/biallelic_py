.. index:: Get started

Get started
===========

Requirements
____________

So `Python` is required to use the software. Only Python3 (Python >= 3.9)
is supported.

Installation
____________

.. note::
   It is strongly advised to use `virtualenv`_ to install the module locally.


From git with pip:
------------------

.. code-block:: bash

   pip install git+https://git@bitbucket.org/weischenfeldt/biallelic_py.git


Using the library:
------------------


After installation the `biallelic_inactivation` command line performs the analysis:

.. command-output:: biallelic_inactivation --help
  :returncode: 0
  :shell:



.. _virtualenv: https://virtualenv.pypa.io
