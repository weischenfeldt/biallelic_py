"""
Prepare the test data

Cleanup temporary folder and assign test
environment variables
"""

import os
from shutil import rmtree


test_dir = os.path.abspath(os.path.join("test", "data"))
tmp_dir = os.path.join(test_dir, "tmp")
try:
    rmtree(tmp_dir)
    os.makedirs(tmp_dir)
except OSError:
    os.makedirs(tmp_dir)
