import unittest
import os
from biallelic.logging import SimpleLogger


class TestLogger(unittest.TestCase):
    def test_logger(self):
        test_str = "Info %i for %s log"
        prog_1 = "main"
        test = SimpleLogger("test", "test/data/tmp")
        test.log.info(test_str % (1, prog_1))
        test.log.info(test_str % (2, prog_1))
        t1 = 0
        with open(
            os.path.join(test.__path__, "%s.log" % "test"), "rt"
        ) as log_main:
            for line in log_main:
                if (
                    test_str % (1, prog_1) in line.rstrip()
                    or test_str % (2, prog_1) in line.rstrip()
                ):
                    t1 += 1
        self.assertEqual(t1, 2)


if __name__ == "__main__":
    unittest.main()
