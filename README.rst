Find Biallelic Inactivation in a sequencing cohort
##################################################

A refactoring of the biallelic_inactivation R implementation
from Nikos

Install and Test
****************

The package can be installed `pip` or by clone and install the
latest version from bitbucket.

By downloading the git repository is also possible to run the test
example data

With Pip
========

.. code-block::

  pip install git+https://bitbucket.org/weischenfeldt/biallelic_py.git


With Git
========


.. code-block::

  git clone https://bitbucket.org/weischenfeldt/biallelic_py.git
  cd biallelic_py
  python3 -m venv venv
  venv/bin/pip install -r requirements.txt
  venv/bin/pip install ../biallelic_py



Test
----

.. code-block::

  venv/bin/biallelic_inactivation \
      test/data/test_cbio/manifest.yaml \
      --output bi_results


